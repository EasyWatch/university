fun main(args: Array<String>) {
    val perceptron = Perceptron(input_count = 2, bias = 0.0,activation_function = ::hardlim)
    perceptron.set_weights(arrayListOf(1.0,-0.8))

    val p = arrayListOf(1.0,2.0)
    val t = 1.0

    println("Моделирование сети до обучения:")
    println(perceptron)
    var a = perceptron.simulate(p) as Double
    println("p = $p")
    println("a = $a")
    println("Ошибка e = ${t-a}")

    println("\nМоделирование сети после обучения:")
    println(perceptron)
    perceptron.learn(arrayListOf(Pair(p,t)))
    println("p = $p")
    a = perceptron.simulate(p) as Double
    println("a = $a")
    println("Ошибка e = ${t-a}")

}



