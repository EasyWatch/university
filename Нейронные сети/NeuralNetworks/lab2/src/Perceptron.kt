import java.util.*


class Perceptron (val input_count:Int, var bias:Double = 0.0, val activation_function:(Double) -> Double, val learning_factor:Double = 1.0) {
    val weights = ArrayList<Double>()
    init {
         for (i in 1..input_count) {
             weights.add(0.0)
         }
     }

    fun simulate (input_vector:List<Double>):Double? {
        if (input_vector.size != input_count) return null
        var sum = bias
        weights.zip(input_vector).forEach {
            val (w_i,p_i) = it
            sum += w_i*p_i
        }
        return activation_function(sum)
    }

    fun set_weights(new_weights:ArrayList<Double>):Boolean {
        if (new_weights.size != weights.size) return false
        new_weights.zip(new_weights.indices).forEach {
            val(w,i) = it
            weights[i] = w
        }
        return true
    }

    fun learn(sample:ArrayList<Pair<ArrayList<Double>,Double>>):Boolean {
        sample.forEach {
            val (p,t) = it
            val a = simulate(p) as Double
            if (t!=a) {
                if (a<t) {
                    bias +=1
                    weights.forEachIndexed { index, weight -> weights[index] = weight+p[index] }
                }
                else {
                    bias -=1
                    weights.forEachIndexed { index, weight -> weights[index] = weight-p[index] }
                }
            }
        }
        return true
    }


    override fun toString(): String {
        var result = ""
        result+="Перспетрон:\n"
        result+=("   Веса: $weights\n")
        result+=("   Смещение: $bias\n")
        result+=("   Функция активации: $activation_function")
        return result
    }


}

fun hardlim(input:Double):Double {
    if (input<0) return 0.0
    return 1.0
}
fun pureline(input:Double):Double {
    return input
}

fun logsig(input:Double):Double {
    return 1.0/(1.0+Math.exp(-input))
}






