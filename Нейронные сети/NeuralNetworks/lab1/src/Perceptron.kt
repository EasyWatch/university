import java.util.*


class Perceptron (val input_count:Int,val offset:Double = 0.0,val activation_function:(Double) -> Double) {
    val weights = ArrayList<Double>()
    init {
         for (i in 1..input_count) {
             weights.add(0.0)
         }
     }

    fun simulate (input_vector:List<Double>):Double? {
        if (input_vector.size != input_count) return null
        var sum = offset
        weights.zip(input_vector).forEach {
            val (w_i,p_i) = it
            sum += w_i*p_i
        }
        return activation_function(sum)
    }

    fun set_weights(new_weights:ArrayList<Double>):Boolean {
        if (new_weights.size != weights.size) return false
        new_weights.zip(new_weights.indices).forEach {
            val(w,i) = it
            weights[i] = w
        }
        return true
    }


    override fun toString(): String {
        var result = ""
        result+="Перспетрон:\n"
        result+=("   Веса: $weights\n")
        result+=("   Смещение: $offset\n")
        result+=("   Функция активации: $activation_function\n")
        return result
    }
}


