fun main(args: Array<String>) {
    val perceptron = Perceptron(input_count = 2,offset = 1.0,activation_function = ::hardlim)
    perceptron.set_weights(arrayListOf(-1.0,1.0))
    println(perceptron)

    val p_1 = arrayListOf(1.0,1.0)
    println("Моделирование сети\n Входной вектор p_1 = $p_1.\n Выход a_1 = ${perceptron.simulate(p_1)}\n")

    val p_2 = arrayListOf(1.0,-1.0)
    println("Моделирование сети\n Входной вектор p_2 = $p_2.\n Выход a_2 = ${perceptron.simulate(p_2)}\n")

}

fun hardlim(input:Double):Double {
    if (input<0) return 0.0
    return 1.0
}

fun pureline(input:Double):Double {
    return input
}

fun logsig(input:Double):Double {
    return 1.0/(1.0+Math.exp(-input))
}



