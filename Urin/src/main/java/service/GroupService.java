package service;


import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.logger.LocalLog;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import model.Group;
import model.User;
import model.UserGroup;

import java.sql.SQLException;
import java.util.List;

public class GroupService {
    private static final String URL = "jdbc:sqlite:/Users/calmarj/db.sqlite";
    private RuntimeExceptionDao<Group,Integer> groupDao;
    private RuntimeExceptionDao<UserGroup,Integer> userGroupDao;

    public GroupService() throws SQLException {
        System.setProperty(LocalLog.LOCAL_LOG_FILE_PROPERTY, "group_log.out");
        ConnectionSource connectionSource = new JdbcConnectionSource(URL);

        groupDao = new RuntimeExceptionDao<>(DaoManager.createDao(connectionSource,Group.class));
        userGroupDao = new RuntimeExceptionDao<>(DaoManager.createDao(connectionSource,UserGroup.class));

    }

    public int createGroup(Group group) {
        return groupDao.create(group);
    }

    public Group getGroupById(Integer id) {
        return groupDao.queryForId(id);
    }

    public int addUserToGroup(Group group, User user) {
        return userGroupDao.create(new UserGroup(user,group));
    }

    public List<UserGroup> getUsersFromGroup(Group group) throws SQLException {
        QueryBuilder<UserGroup,Integer> queryBuilder = userGroupDao.queryBuilder();
        return queryBuilder.where().eq("group_id",group.getId()).query();
    }

    public List<Group> getAllGroups() {
        return groupDao.queryForAll();
    }

    public int deleteGroup(Group group) throws SQLException {
        int i = groupDao.delete(group);
        if (i == 1) {
            DeleteBuilder<UserGroup,Integer> deleteBuilder = userGroupDao.deleteBuilder();
            deleteBuilder.where().eq("group_id",group.getId());
            return deleteBuilder.delete();
        }
        return 0;
    }

    public int removeUserFromGroup(User user,Group group) throws SQLException {
        DeleteBuilder<UserGroup,Integer> deleteBuilder = userGroupDao.deleteBuilder();
        deleteBuilder.where().eq("group_id",group.getId()).and().eq("user_id",user.getLogin());
        return deleteBuilder.delete();
    }


}
