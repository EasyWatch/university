package service;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import model.Group;
import model.Message;
import model.User;
import model.UserGroup;

import java.sql.SQLException;


public class DBService {
    private static final String URL = "jdbc:sqlite:/Users/calmarj/db.sqlite";

    public static void prepareDB() throws SQLException {
        ConnectionSource connectionSource = new JdbcConnectionSource(URL);


        TableUtils.dropTable(connectionSource, User.class,false);
        /*TableUtils.dropTable(connectionSource, Message.class,false);
        TableUtils.dropTable(connectionSource, Group.class,false);
        TableUtils.dropTable(connectionSource, UserGroup.class,false);*/


        TableUtils.createTable(connectionSource, User.class);
       /* TableUtils.createTable(connectionSource, Message.class);
        TableUtils.createTable(connectionSource, Group.class);
        TableUtils.createTable(connectionSource, UserGroup.class);*/
    }
}
