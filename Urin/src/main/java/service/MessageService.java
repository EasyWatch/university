package service;


import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.logger.LocalLog;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import model.Message;
import model.User;

import java.sql.SQLException;
import java.util.List;

public class MessageService {
    private static final String URL = "jdbc:sqlite:/Users/calmarj/db.sqlite";
    private RuntimeExceptionDao<Message,Integer> dao;

    public MessageService() throws SQLException {
        System.setProperty(LocalLog.LOCAL_LOG_FILE_PROPERTY, "message_log.out");
        ConnectionSource connectionSource = new JdbcConnectionSource(URL);
        Dao<Message,Integer> localDao = DaoManager.createDao(connectionSource,Message.class);
        dao = new RuntimeExceptionDao<>(localDao);
    }

    public int create(Message message) {
        return dao.create(message);
    }

    public List<Message> getAllMessages(User user) throws SQLException {
        QueryBuilder<Message,Integer> queryBuilder = dao.queryBuilder();
        return queryBuilder.where().eq("to_user",user.getLogin()).query();
    }

    public List<Message> getMessageById(User user, Integer id) throws SQLException {
        QueryBuilder<Message,Integer> queryBuilder = dao.queryBuilder();
        return queryBuilder.where().eq("to_user",user.getLogin()).and().eq("id",id).query();
    }

    public int update(Message message) {
        return dao.update(message);
    }
}
