package service;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.logger.LocalLog;
import com.j256.ormlite.support.ConnectionSource;
import model.User;

import java.sql.SQLException;
import java.util.List;


public class UserService {
    private static final String URL = "jdbc:sqlite:/Users/calmarj/db.sqlite";
    private RuntimeExceptionDao<User,String> dao;

    public UserService( ) throws SQLException {
        System.setProperty(LocalLog.LOCAL_LOG_FILE_PROPERTY, "user_log.out");
        ConnectionSource connectionSource = new JdbcConnectionSource(URL);
        Dao<User,String> localDao = DaoManager.createDao(connectionSource,User.class);
        dao = new RuntimeExceptionDao<>(localDao);
    }

    public int create(User user) throws SQLException {
        if (dao.idExists(user.getLogin())) {
            throw new SQLException("Пользователь с таким логином уже существует");
        }
        return dao.create(user);
    }

    public int update(User user) throws SQLException {
        return dao.update(user);
    }

    public int delete(User user) throws SQLException {
        return dao.delete(user);
    }

    public User getUserByLogin(String login) throws SQLException
    {
        return dao.queryForId(login);
    }

    public List<User> getAll() throws Exception {
        return dao.queryForAll();
    }

}
