import manager.GroupManager;
import manager.UserManager;
import model.User;
import util.MyUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class AdminProgram {
    private static final Scanner INPUT = new Scanner(System.in);
    private static UserManager userManager;
    private static GroupManager groupManager;

    public static void main(String[] args) throws Exception {
       // DBService.prepareDB();
        userManager = new UserManager();
        groupManager = new GroupManager();
        while (true) {
            User currentAdmin = login();
            if (currentAdmin !=null) {
                break;
            }
        }
        admin();
    }
    private static User login() throws SQLException {
        System.out.print("Введите логин:");
        String login = INPUT.nextLine();
        String password = MyUtils.readPassword();

        User currentUser = userManager.getUserByLogin(login);

        if (currentUser != null&&currentUser.getPassword().equals(password)) {
            if (currentUser.isAdmin()) {
                return currentUser;
            }
            else {
                System.out.println("У вас недостаточно прав для доступа в администраторскую программу.");
                return null;
            }
        }
        System.out.println("Неправильно введены логин/пароль\n");
        return null;
    }

    private static void admin() throws Exception {
        while(true) {
            System.out.print("\nВыберите действие:\n\t1. Работа с пользователями\n\t2. Работа с группами. \n\t3. Выйти" +
                    "\nВведите номер действия:");

            int choice = INPUT.nextInt();

            switch (choice) {
                case 1: {
                    workWithUsers();
                    break;
                }
                case 2: {
                    workWithGroups();
                    break;
                }
                case 3: {
                    System.out.println("Удачного дня!");
                    return;
                }
            }
        }
    }

    private static void workWithGroups() throws IOException, InterruptedException, SQLException {
        while (true) {
            System.out.print("\nРабота с группами пользователей.\nВыберите действие:\n\t1. Создать группу\n\t2. Добавить пользователя в группу\n\t3. Удалить пользователя из группы\n\t4. Удалить группу\n\t5. Вернуться назад\nВведите номер действия:");
            int choice = INPUT.nextInt();
            switch (choice) {
                case 1: {
                    groupManager.createGroup();
                    break;
                }
                case 2: {
                    groupManager.addUserToGroup();
                    break;
                }
                case 3: {
                    groupManager.removeUserToGroup();
                    break;
                }
                case 4: {
                    groupManager.deleteGroup();
                    break;
                }
                case 5: {
                    return;
                }
            }
            Thread.sleep(1000);
        }
    }

    private static void workWithUsers() throws Exception {
        while (true) {
            System.out.print("\nРабота с пользователями.\nВыберите действие:\n\t1. Создать пользователя\n\t2. Модифицировать пользователя\n\t3. Удалить пользователя\n\t4. Вывести список пользователей\n\t5. Вернуться назад\nВведите номер действия:");
            int choice = INPUT.nextInt();
            switch (choice) {
                case 1: {
                    userManager.createUser();
                    break;
                }
                case 2: {
                    userManager.updateUser();
                    break;
                }
                case 3: {
                    userManager.deleteUser();
                    break;
                }
                case 4: {
                    userManager.getAllUsers();
                    break;
                }
                case 5: {
                    return;
                }
            }
            Thread.sleep(1000);
        }
    }



}
