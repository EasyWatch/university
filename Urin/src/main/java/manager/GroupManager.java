package manager;


import model.Group;
import model.User;
import model.UserGroup;
import org.jetbrains.annotations.Nullable;
import service.GroupService;
import service.UserService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GroupManager {
    private GroupService groupService;
    private UserService userService;

    private BufferedReader input =new BufferedReader(new InputStreamReader(System.in));

    public GroupManager() throws SQLException {
        this.groupService = new GroupService();
        this.userService = new UserService();
    }

    public void createGroup() throws IOException {
        System.out.println("Создание группы.");
        System.out.print("Введите имя группы:");
        String name = input.readLine();

        Group group = new Group(name);

        if (groupService.createGroup(group) == 1) {
            System.out.println("Создание группы прошло успешно");
        }
        else {
            System.out.println("Создание группы провалилось");
        }
    }

    public void addUserToGroup() throws IOException, SQLException {
        System.out.println("Добавление пользователя в группу.");
        Group group = getGroupFromInput();
        if (group == null) return;
        System.out.print("Введите имя пользователя для добавления: ");
        String login = input.readLine();
        User user = userService.getUserByLogin(login);
        if (user == null) {
            System.err.println("Такого пользователя не существует");
            return;
        }

        if (groupService.addUserToGroup(group,user) == 1) {
            System.out.println("Добавление пользователя прошло успешно");
        }
        else {
            System.out.println("Добавление пользователя провалилось");
        }

    }

    public void removeUserToGroup() throws IOException, SQLException {
        System.out.println("Удаление пользователя из группы.");

        Group group = getGroupFromInput();
        if (group == null) return;
        listUsersFromGroup(group);

        System.out.print("Введите имя пользователя для удаления:");
        String login = input.readLine();
        User user = userService.getUserByLogin(login);
        if (user == null) {
            System.err.println("Такого пользователя не существует");
            return;
        }
        if (groupService.removeUserFromGroup(user,group) == 1) {
            System.out.println("Удаление пользователя прошло успешно");
        }
        else {
            System.out.println("Удаление пользователя провалилось");
        }
    }

    public void deleteGroup() throws IOException, SQLException {
        System.out.println("Удаление группы.");

        Group group = getGroupFromInput();
        if (group == null)return;

        if (groupService.deleteGroup(group) !=0) {
            System.out.println("Удаление группы прошло успешно");
        }else
        {
            System.out.println("Удаление группы провалилось");
        }
    }

    private void listGroups() {
        System.out.println("Список групп:");
        List<Group> groups = groupService.getAllGroups();
        for (Group group : groups) {
            System.out.println("\t"+group);
        }
    }

    @Nullable
    public Group getGroupFromInput() throws IOException {
        listGroups();
        System.out.print("Введите номер группы:");
        Integer groupId = Integer.valueOf(input.readLine());
        Group group = groupService.getGroupById(groupId);
        if (group == null) {
            System.err.println("Такой группы не существует");
            return null;
        }
        return group;
    }

    public List<User> listUsersFromGroup(Group group) throws SQLException {
        System.out.println("Пользователи в "+group);
        List<UserGroup> userGroups = groupService.getUsersFromGroup(group);
        List<User> users = new ArrayList<>();
        for (UserGroup userGroup : userGroups) {
            System.out.println("\t"+userGroup.getUser());
            users.add(userGroup.getUser());
        }
        return users;

    }

}
