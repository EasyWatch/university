package manager;


import model.Group;
import model.Message;
import model.User;
import org.jetbrains.annotations.Nullable;
import service.GroupService;
import service.MessageService;
import service.UserService;
import util.MyUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class MessageManager {
    private MessageService messageService;
    private UserService userService;

    private GroupManager groupManager;

    private BufferedReader input =new BufferedReader(new InputStreamReader(System.in));

    public MessageManager() throws SQLException {
        messageService = new MessageService();
        userService = new UserService();
        groupManager = new GroupManager();
    }

    public void sendMessageToUser(User fromUser) throws SQLException, IOException {
        System.out.println("Отправка сообщения пользователю.");

        System.out.print("Введите пользователя, которому отправить сообщение:");
        String toUserLogin = input.readLine();

        User toUser  = userService.getUserByLogin(toUserLogin);
        if (toUser == null) {
            System.err.println("Такого пользователя не существует.");
            return;
        }

        System.out.print("Введите текст сообщения:");
        String text = input.readLine();

        System.out.println("Выберите гриф секретности.");
        int i = 0;
        for (Message.SecretLevel secretLevel : Message.SecretLevel.values()) {
            System.out.println(String.format("\t№%d %s",(i++),secretLevel.getLevel()));
        }
        System.out.print("Введите номер грифа секретности:");
        int secretLevelIndex = Integer.parseInt(input.readLine());
        String secretLevel = Message.SecretLevel.values()[secretLevelIndex].name();

        Message message = new Message(text,new Date(),fromUser,toUser,secretLevel);

        if (messageService.create(message) == 1) {
            System.out.println("Сообщение успешно отправлено");
        }
        else {
            System.out.println("Сообщение не отправлено");
        }
    }

    public void sentMessageToGroup(User fromUser) throws IOException, SQLException {
        System.out.println("Отправка сообщения группе пользователей.");

        Group group = groupManager.getGroupFromInput();
        if (group == null) return;

        List<User> users = groupManager.listUsersFromGroup(group);

        System.out.print("Введите текст сообщения:");
        String text = input.readLine();

        System.out.println("Выберите гриф секретности.");
        int i = 0;
        for (Message.SecretLevel secretLevel : Message.SecretLevel.values()) {
            System.out.println(String.format("\t№%d %s",(i++),secretLevel.getLevel()));
        }
        System.out.print("Введите номер грифа секретности:");
        int secretLevelIndex = Integer.parseInt(input.readLine());
        String secretLevel = Message.SecretLevel.values()[secretLevelIndex].name();

        for (User user : users) {
            Message message = new Message(text,new Date(),fromUser,user,secretLevel);
            messageService.create(message);
        }

    }

    public void getAllMessages(User user) throws SQLException {
        System.out.println("Список сообщений.");

        List<Message> messages = messageService.getAllMessages(user);
        messages.forEach(System.out::println);

    }

    public void readMessage(User user) throws SQLException, IOException {
        System.out.print("Введите номер сообщения для прочтения:");
        int messageIndex = Integer.parseInt(input.readLine());

        List<Message> messages = messageService.getMessageById(user,messageIndex);
        if (messages.size()<1) {
            System.err.println("Сообщения с таким номером для данного пользователя не существует");
            return;
        }

        Message message = messages.get(0);
        MyUtils.printMessage(message);
        message.setRead(true);
        messageService.update(message);

    }



}
