package manager;

import model.User;
import service.UserService;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class UserManager {
    private static final Scanner INPUT = new Scanner(System.in);

    private UserService userService;

    public UserManager() throws SQLException {
        userService = new UserService();
    }


    public void createUser() {
        System.out.println("\n\nСоздание пользователя.");
        System.out.print("Введите логин пользователя: ");
        String login = INPUT.nextLine().trim();
        System.out.print("Введите пароль пользователя: ");
        String password = INPUT.nextLine().trim();

        try {
            userService.create(new User(login,password));
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.err.println("Создание пользователя провалилось");
            return ;
        }

        System.out.println("Создание пользователя прошло успешно");
        return;
    }

    public void updateUser() throws SQLException {
        System.out.println("Модификация пользователя.");
        System.out.print("Введите логин пользователя для модификации:");
        String oldLogin = INPUT.nextLine();
        User user = userService.getUserByLogin(oldLogin);
        if (user==null) {
            System.err.println("Пользователя с таким логином не существует");
            return ;
        }
        System.out.print("Введите новый логин пользователя:");
        String newLogin = INPUT.nextLine();
        user.setLogin(newLogin);
        System.out.println("Введите новый пароль пользователя:");
        String newPassword = INPUT.nextLine();
        user.setPassword(newPassword);
        if (userService.update(user)==1) {
            System.out.printf("Модификация прошла успешно");
        }
        else
        {
            System.out.println("Модификация провалилась");
        }

    }

    public void deleteUser() throws SQLException {
        System.out.println("Удаление пользователя.");

        System.out.print("Введите логин пользователя для удаления:");
        String login = INPUT.nextLine();
        User user = userService.getUserByLogin(login);
        if (user==null) {
            System.err.println("Пользователя с таким логином не существует");
            return;
        }

        if (userService.delete(user)==1) {
            System.out.printf("Удаление прошло успешно");
        }
        else
        {
            System.out.println("Удаление провалилось");
        }
    }

    public User getUserByLogin(String login) throws SQLException {
        return userService.getUserByLogin(login);
    }


    public void getAllUsers() throws Exception {
        List<User> users = userService.getAll();
        System.out.println("\nСписок пользователей.");
        for (User user : users) {
            System.out.println(user.getLogin());
        }
    }
}
