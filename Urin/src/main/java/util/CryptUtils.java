package util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class CryptUtils {
    private static final String ALGORITHM = "AES";
    private static final int BIT_SIZE = 512;
    private static final BigInteger MODULE = new BigInteger("146070341687473608625747426401119605367358535001005255452939855107822633398602023068336861155002420306029715940490580575160018364073877682816069111438233488236784448057831921373716294240760578686473291279966813889734979721380465770218057739403975098971374049110240240736422543504950860869279781786203666213661");

    public static void generateKeys(int n,String directory) throws NoSuchAlgorithmException, IOException {
        final SecureRandom secureRandom = SecureRandom.getInstanceStrong();
        BigInteger remainder = new BigInteger(BIT_SIZE,secureRandom);
        for (int i = 0; i < n; i++) {
            BigInteger multiplier = new BigInteger(BIT_SIZE,secureRandom);
            BigInteger randomKey = MODULE.multiply(multiplier).add(remainder);
            MyUtils.printToFile(CryptUtils.encodeStringWithBase64(randomKey.toString()), directory+"/key" + (i + 1) + ".txt");
        }
    }

    public static void encrypt(File openTextFile, File keyFile,File outputFile) throws IOException {
        String openText = MyUtils.getInputFromFile(openTextFile);
        String stringKey = decodeStringWithBase64(MyUtils.getInputFromFile(keyFile));

        BigInteger key = new BigInteger(stringKey).mod(MODULE);

        String encryptedText = aesEncrypt(openText,key.toString());

        MyUtils.printToFile(encryptedText,outputFile.getAbsolutePath());
    }

    public static void decrypt(File encryptedTextFile, File keyFile,File outputFile) throws IOException {
        String encryptedText = MyUtils.getInputFromFile(encryptedTextFile);
        String stringKey = decodeStringWithBase64(MyUtils.getInputFromFile(keyFile));

        BigInteger key = new BigInteger(stringKey).mod(MODULE);

        String decryptedText = aesDecrypt(encryptedText,key.toString());

        MyUtils.printToFile(decryptedText,outputFile.getAbsolutePath());
    }

    private static String aesDecrypt(final String encryptedVal, final String secretKey) {
        String decryptedValue = null;

        try {

            final Key key = generateKeyFromString(secretKey);
            final Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.DECRYPT_MODE, key);
            final byte[] decorVal = new BASE64Decoder().decodeBuffer(encryptedVal);
            final byte[] decValue = c.doFinal(decorVal);
            decryptedValue = new String(decValue);
        } catch(Exception ex) {
            System.out.println("ОШИБКА: " + ex);
        }

        return decryptedValue;
    }

    private static String aesEncrypt(final String valueEnc, final String secKey) {
        String encryptedVal = null;

        try {
            final Key key = generateKeyFromString(secKey);
            final Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.ENCRYPT_MODE, key);
            final byte[] encValue = c.doFinal(valueEnc.getBytes());
            encryptedVal = new BASE64Encoder().encode(encValue);
        } catch(Exception ex) {
            System.out.println("ОШИБКА: " + ex);
        }

        return encryptedVal;
    }

    private static Key generateKeyFromString(final String secKey) throws Exception {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] bytesOfMessage = secKey.getBytes("UTF-8");
        byte[] keyVal = md.digest(bytesOfMessage);
        return new SecretKeySpec(keyVal, ALGORITHM);
    }

    public static String encodeStringWithBase64(String string) throws UnsupportedEncodingException {
        byte[] bytesOfString = string.getBytes("UTF-8");

        return new BASE64Encoder().encodeBuffer(bytesOfString);
    }

    public static String decodeStringWithBase64(String string) throws IOException {

        final byte[] bytes = new BASE64Decoder().decodeBuffer(string);
        return new String(bytes);
    }


}
