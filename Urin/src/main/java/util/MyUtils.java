package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class MyUtils {
    public static String getInputFromFile(File file) {
        try {
            Scanner scanner = new Scanner(file);
            StringBuilder stringBuilder = new StringBuilder();
            while (scanner.hasNextLine())
            {
                stringBuilder.append(scanner.nextLine()).append("\n");
            }
            return stringBuilder.toString();
        } catch (FileNotFoundException e) {
            System.err.println(String.format("WARN!! Файл \'%s\' не был найден",file.getPath()));
            System.exit(-1);
        }
        return null;
    }

    public static void printToFile(String text, String filename) throws IOException {
        final File file = new File(filename);
        if(!file.exists()) {
            file.createNewFile();
        }
        PrintWriter printWriter = new PrintWriter(file);
        printWriter.println(text);
        printWriter.flush();
        printWriter.close();
    }
}
