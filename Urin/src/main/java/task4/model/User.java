package task4.model;

import org.hibernate.annotations.GenericGenerator;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.List;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
@Entity
@Table(name = "user")
public class User
{

	private int           id;
	private String        username;
	private String        password;
	private boolean       isAdmin;
	private List<Message> messages;

	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public int getId()
	{
		return id;
	}

	@Basic
	@Column(name = "username")
	public String getUsername()
	{
		return username;
	}

	@Basic
	@Column(name="password")
	public String getPassword()
	{
		return password;
	}

	@Basic
	@Column(name="is_admin")
	public boolean isAdmin()
	{
		return isAdmin;
	}

	public List<Message> getMessages()
	{
		return messages;
	}

	public void setIsAdmin(boolean isAdmin)
	{
		this.isAdmin = isAdmin;
	}

	public User()
	{
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public void setMessages(List<Message> messages)
	{
		this.messages = messages;
	}
}
