package task4.model;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Message
{
	private String text;
	private boolean isRead;
	private String secretLevel;

	private User fromUser;
	private User toUser;


	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public boolean isRead()
	{
		return isRead;
	}

	public void setIsRead(boolean isRead)
	{
		this.isRead = isRead;
	}

	public String getSecretLevel()
	{
		return secretLevel;
	}

	public void setSecretLevel(String secretLevel)
	{
		this.secretLevel = secretLevel;
	}
}
