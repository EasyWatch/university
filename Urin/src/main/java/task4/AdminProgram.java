package task4;

import task4.manager.UserManager;

import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class AdminProgram
{

	private static final Scanner INPUT = new Scanner( System.in );

	public static void main(String[] args)
	{
		if (login())
		{
			try
			{
				System.out.println( "Вы вошли в Админ 1.0 под администратором" );

				while (true)
				{
					System.out.println(
									"Выберите действие:\n\t"
													+ "1. Работа с пользователями.\n\t"
													+ "2. Работа с группами.\n\t"
													+ "3. Выйти из системы.\n"
													+ "Введите норм действия:" );

					int choice = INPUT.nextInt();

					switch (choice)
					{
						case 1:
						{
							workWithUsers();
							break;
						}
						case 2:
						{
							workWithGroups();
							break;
						}
						case  3:
						{
							System.out.println("Пока! Удачного дня");
							System.exit( -1 );
						}
						default:
						{
							System.out.println( "Нет такого действия! Попробуйте снова" );
						}
					}
				}
			}
			catch (Exception ex) {
				System.err.println( "Ошибочка" );
			}
		}
		else
		{
			System.out.println( "Сосай" );
		}
	}

	private static void workWithGroups()
	{
		System.out.println("Работа с пользователями:");
		System.out.println();
	}

	private static void workWithUsers()
	{
		while (true)
		{
			System.out.println(
							"Выберите действие:\n\t"
											+ "1. Создать пользователя.\n\t"
											+ "2. Изменить пользователя.\n\t"
											+ "3. Удалить пользователя.\n\t"
											+ "4. Вывести всех пользовтелей системы.\n\t"
											+ "5. Вернуться назад. \n"
											+ "Введите номер действия:" );
			int choice = INPUT.nextInt();
			switch (choice) {
				case 1:
				{

				}
				case 2:
				{

				}
				case 3:
				{

				}
				case 4:
				{

				}
				case 5:
				{
					return;
				}
			}
		}

	}

	private static boolean login()
	{
		System.out.print( "Добро пожаловать в Админ 1.0\nВведите логин администратора: " );

		String login = INPUT.nextLine();
		String password = UserManager.inputPassword();

		return (login.equals( "admin" ) && password.equals( "123456" ));
	}


}
