package task4.manager;

import task4.model.User;

import java.io.Console;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class UserManager
{
	public static User createUser(Scanner input) {
		System.out.print( "Введите логин пользователя:" );
		String login = input.next();
		String password = inputPassword();

		return new User();
	}

	public static String inputPassword()
	{
		Console console = System.console();
		if (console == null)
		{
			System.out.println( "Невозможно получить Сonsole instance" );
			System.exit( 0 );
		}

		char passwordArray[] = console.readPassword( "Введите пароль: " );
		return new String( passwordArray );
	}
}
