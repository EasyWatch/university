import manager.GroupManager;
import manager.MessageManager;
import manager.UserManager;
import model.User;
import service.DBService;
import util.MyUtils;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class UserProgram {

    private static final Scanner INPUT = new Scanner(System.in);
    private static UserManager userManager;

    private static User currentUser;

    public static void main(String[] args) throws SQLException, IOException {
       // DBService.prepareDB();
        userManager = new UserManager();
        System.out.println("Добро пожаловать в пользовательскую часть программы.");

        while (true) {
            currentUser =login();
            if (currentUser!=null) {
                break;
            }
        }
        user();
    }

    private static User login() throws SQLException {
        System.out.print("Введите логин:");
        String login = INPUT.nextLine();
        String password = MyUtils.readPassword();

        User currentUser = userManager.getUserByLogin(login);

        if (currentUser != null&&currentUser.getPassword().equals(password)) {
            return currentUser;
        }
        System.out.println("Неправильно введены логин/пароль\n");
        return null;
    }

    private static void user() throws SQLException, IOException {
        MessageManager messageManager = new MessageManager();
        while(true) {
            System.out.print("Уважаемый "+currentUser.getLogin()+", выберите действие:\n\t" +
                    "1. Отправить сообщение пользователю.\n\t" +
                    "2. Отправить сообщение группе пользователей.\n\t"+
                    "3. Посмотреть сообщения. \n\t" +
                    "4. Выйти\n" +
                    "Введите номер действия:");

            int choice = INPUT.nextInt();

            switch (choice) {
                case 1: {
                    messageManager.sendMessageToUser(currentUser);
                    break;
                }
                case 2: {
                    messageManager.sentMessageToGroup(currentUser);
                    break;
                }
                case 3: {
                    messageManager.getAllMessages(currentUser);
                    messageManager.readMessage(currentUser);
                    break;
                }
                case 4: {
                    System.out.println("Удачного дня!");
                    return;
                }
            }
        }
    }

}
