package model;

import com.j256.ormlite.field.DatabaseField;


public class UserGroup {
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign=true)
    private User user;

    @DatabaseField(foreign = true)
    private Group group;

    public UserGroup() {
    }

    public UserGroup(User user, Group group) {
        this.user = user;
        this.group = group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
