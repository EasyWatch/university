package model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import util.MyUtils;

import java.util.Date;

@DatabaseTable
public class Message {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String text;
    @DatabaseField
    private Date date;
    @DatabaseField(columnName = "from_user",foreign = true)
    private User from;
    @DatabaseField(columnName = "to_user",foreign = true)
    private User to;
    @DatabaseField
    private SecretLevel secretLevel;
    @DatabaseField(columnName = "is_read")
    private boolean read;

    public Message() {
    }

    public Message(String text, Date date, User from, User to, String secretLevel) {
        this.text = text;
        this.date = date;
        this.from = from;
        this.to = to;
        this.secretLevel = SecretLevel.valueOf(secretLevel);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public SecretLevel getSecretLevel() {
        return secretLevel;
    }

    public void setSecretLevel(SecretLevel secretLevel) {
        this.secretLevel = secretLevel;
    }

    private boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }


    @Override
    public String toString() {
        return String.format("№%d (%s) Сообщение от %s (%s)",id,MyUtils.formatDate(date),from,((isRead())?"Прочитано":"Не прочитано"));
    }

    public enum SecretLevel {
        SECRET("Секретно"), SUPER_SECRET("Совершенно секретно"), NO_SECRET("Не секретно");

        private String level;

        SecretLevel(String level) {
            this.level = level;
        }

        public String getLevel() {
            return level;
        }
    }
}
