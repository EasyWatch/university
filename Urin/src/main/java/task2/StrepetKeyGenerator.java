package task2;

import util.CryptUtils;

import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class StrepetKeyGenerator {

    private static final String KEY_DIRECTORY = "/Users/estrepetov/University/course_5/university/Urin/src/main/resources/keys";

    public static void main(String[] args) throws NoSuchAlgorithmException {

        try {
            System.out.print("Введите количество генерируемых ключей:");
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            CryptUtils.generateKeys(n, KEY_DIRECTORY);
            System.out.println("Генерация ключей успешно законечена");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.out.println("Произошла ошибка во время работы программы. Попробуйте снова!");
        }
    }


}
