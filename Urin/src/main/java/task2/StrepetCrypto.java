package task2;

import util.CryptUtils;

import java.io.File;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class StrepetCrypto {
    public static void main(String[] args) {
        System.out.print("Выберите операцию:\n\t1) Зашифровать файл.\n\t2) Расшифровать файл.\nВведите номер операции:");
        Scanner scanner = new Scanner(System.in);

        int choice = scanner.nextInt();
        scanner.nextLine();

        switch (choice) {
            case 1: {
                strepetEncrypt();
                break;
            }
            case 2: {
                strepetDecrypt();
                break;
            }
            default: {
                System.out.println("Ошибка! Неправильно выбранная опция");
                System.exit(-1);
            }
        }
    }

    private static void strepetEncrypt() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите путь до файла с ключом:");
        File keyFile = new File(scanner.nextLine());
        checkFileExistence(keyFile);

        System.out.print("Введите путь до файла с открытым текстом:");
        File openTextFile = new File(scanner.nextLine());
        checkFileExistence(openTextFile);

        System.out.print("Введите путь до файла для зашифрованного текста:");
        File encryptedTextFile = new File(scanner.nextLine());

        try {
            CryptUtils.encrypt(openTextFile,keyFile,encryptedTextFile);
            System.out.println("Шифрование прошло успешно");
        } catch (Exception ex) {
            System.err.println("Произошла ошибка при шифровании");
            System.exit(-1);
        }

    }

    private static void strepetDecrypt() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите путь до файла с ключом:");
        File keyFile = new File(scanner.nextLine());
        checkFileExistence(keyFile);

        System.out.print("Введите путь до файла с зашифрованным текстом:");
        File encryptedTextFile = new File(scanner.nextLine());
        checkFileExistence(encryptedTextFile);

        System.out.print("Введите путь до файла для расшифрованного текста:");
        File decryptedTextFile = new File(scanner.nextLine());

        try {
            CryptUtils.decrypt(encryptedTextFile, keyFile,decryptedTextFile);
            System.out.println("Расшифрование прошло успешно");
        } catch (Exception ex) {
            System.err.println("Произошла ошибка при расшифровании");
            System.exit(-1);
        }
    }


    private static void checkFileExistence(File file) {
        if (!file.exists()) {
            System.err.println("Ошибка! Такой файл не существует");
            System.exit(-1);
        }
    }
}
