package service;

import model.User;

import java.util.List;

/**
 * Created by calmarj on 09.05.16.
 */
public class UserServiceTest {
    public static void main(String[] args) throws Exception {
       // System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "ERROR");
        UserService userService = new UserService();
        final User user1 = new User("login", "password");
        userService.create(user1);
        List<User> users = userService.getAll();
        for (User user : users) {
            System.out.println(user.getLogin());
        }
    }
}
