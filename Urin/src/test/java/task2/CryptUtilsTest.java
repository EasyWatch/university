package task2;

import org.junit.Assert;
import org.junit.Test;
import util.CryptUtils;
import util.MyUtils;

import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class CryptUtilsTest {
public static final String KEY_DIRECTORY = "/Users/estrepetov/University/course_5/university/Urin/src/main/resources/keys";

    @Test
    public void testBase64Encoding() throws IOException {
        String openText = "Тестовая фраза";
        String encodedText = CryptUtils.encodeStringWithBase64(openText);
        String decodedText = CryptUtils.decodeStringWithBase64(encodedText);

        Assert.assertEquals("Текст после декодирования должен быть таким как и открытый текст",openText,decodedText);
    }
    @Test
    public void testEncryption() throws IOException {
        File openTextFile = new File("/Users/estrepetov/University/course_5/university/Urin/src/test/java/task2/testOpenText.txt");
        File encryptedTextFile = new File("/Users/estrepetov/University/course_5/university/Urin/src/test/java/task2/testEncText.txt");
        File decryptedTextFile = new File("/Users/estrepetov/University/course_5/university/Urin/src/test/java/task2/testDecText.txt");

        Random random = new Random();

        int encryptKeyIndex = random.nextInt(4)+1;
        int decryptKeyIndex = random.nextInt(4)+1;

        File encryptionKeyFile = new File(KEY_DIRECTORY+"/key"+encryptKeyIndex+".txt");
        File decryptionKeyFile = new File(KEY_DIRECTORY+"/key"+decryptKeyIndex+".txt");

        CryptUtils.encrypt(openTextFile,encryptionKeyFile,encryptedTextFile);

        CryptUtils.decrypt(encryptedTextFile,decryptionKeyFile,decryptedTextFile);

        String openText = MyUtils.getInputFromFile(openTextFile);
        String decryptedText = MyUtils.getInputFromFile(decryptedTextFile);

        Assert.assertEquals("Расшифрованный текст должен совпадать с открытым",openText.trim(),decryptedText.trim());

    }
}
