package model

import java.util.*

class Directory(name:String, var files:List<File> = ArrayList<File>()): File(name) {
    override fun toString(): String = "Директория \'$name\'"
}