package model

import hru.Subject
import java.io.Serializable

class User(name:String="",password:String="",val isAdmin:Boolean = false): Subject(name,password),Serializable {
    private val serialVersionUID = 1947776278070329562

    override fun toString(): String {
        val string = if (isAdmin) "Администратор" else "Пользователь"
        return string+" \'$name\'"
    }
}