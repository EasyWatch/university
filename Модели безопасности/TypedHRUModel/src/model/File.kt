package model

import hru.Object

open class File(name:String,var isAdminable:Boolean = false) : Object(name) {
    override fun toString(): String = "Файл \'$name\'"
}