import hru.AccessMatrix
import hru.ModelType
import hru.Object
import hru.Right
import model.Directory
import model.File
import model.User
import java.io.Serializable
import java.util.*
import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

class Manager : Serializable {

    val accessMatrix: AccessMatrix = AccessMatrix()

    val directories: HashMap<String, Directory> = HashMap()

    val admins: HashSet<User> = HashSet()

    val users: HashSet<User> = HashSet()

    val typeGraph: ArrayList<ArrayList<Boolean>> = ArrayList()

    companion object {
        var logger = Logger.getLogger(this.toString())
    }

    //init manager of system
    init {
        logger.useParentHandlers = false
        val handler = FileHandler("manager_logs", true)
        val formatter = SimpleFormatter()
        handler.formatter = formatter;
        logger.addHandler(handler)
        logger.level = Level.SEVERE

        initTypeGraph()
    }


    //create functions
    fun createUser(name: String, password: String, t: ModelType = ModelType.USER): User? {
        val user = User(name, password)
        user.type = t

        if (accessMatrix.createSubject(user)) {
            logger.info("Успешно создан \'${user}\'")
            users.add(user)
            return user
        } else {
            logger.severe("Ошибка! создание \'${user}\' провалилось")
            return null
        }
    }

    fun createFile(user: User, directoryName: String, fileName: String,t:ModelType = ModelType.NONE): File? {


        val file = File(fileName, user.isAdmin)
        val directory = directories[directoryName]

        if (directory != null) {
            if (directory.isAdminable) {
                if (!user.isAdmin) {
                    logger.severe("Ошибка! Попытка создания в папки администратора не администратором")
                    return null
                }
            }

            if (accessMatrix.checkRight(Right.WRITE, user, o = directory)) {
                accessMatrix.createObject(file)
                accessMatrix.enter(Right.OWN, user, file)
                giveWRERights(user, file)
                directory.files += file
                admins.forEach { admin -> giveWRERights(admin, file) }
                return file
            } else {
                logger.severe("Ошибка! У \'$user\' нет права записи в \'$directory\'")
            }
        }
        return null
    }

    fun createDirectory(user: User, directoryName: String, type: ModelType = ModelType.NONE): Directory? {

        val directory = Directory(directoryName)
        accessMatrix.createObject(directory)
        directories.put(directoryName, directory)
        accessMatrix.enter(Right.OWN, user, directory)
        giveWRERights(user, directory)
        admins.forEach { admin -> giveWRERights(admin, directory) }
        logger.info("\'$user\' успешно создал \'$directory\'")
        return directory
    }


    //functions with rights
    fun checkRight(user:User,o:Object,right: Right):Boolean {
        return accessMatrix.checkRight(right,user,o)
    }

    fun checkWRERights(user: User, o: Object): Boolean {
        return (accessMatrix.checkRight(Right.READ, user, o)) &&
                (accessMatrix.checkRight(Right.WRITE, user, o)) &&
                (accessMatrix.checkRight(Right.EXECUTE, user, o))
    }

    fun checkAllRights(user:User,o:Object):Boolean {
        return checkWRERights(user,o)&&checkRight(user,o, Right.OWN)
    }

    fun giveWRERights(user: User, o: Object) {
        accessMatrix.enter(Right.READ, user, o)
        accessMatrix.enter(Right.WRITE, user, o)
        accessMatrix.enter(Right.EXECUTE, user, o)
    }

    fun giveRightFromOwner(owner: User, user: User, file: File, right: Right):Boolean {
        if (accessMatrix.checkRight(Right.OWN, owner, file)&&right != Right.OWN) {
            accessMatrix.enter(right, user, file)
            return true
        }
        return false
    }



    //optional functions
    fun copyFile(user: User, file: File, directoryName: String, newFileName: String): File? {
        val directory = directories[directoryName]

        if (directory != null) {
            if ((file.isAdminable && !user.isAdmin)) {
                logger.severe("Ошибка! Попытка не администратора \'$user\' скопировать администраторский \'$file\' не удалась")
                return null
            } else {
                if (accessMatrix.checkRight(Right.READ, user, file)) {
                    val newFile = createFile(user, directoryName, newFileName) as File
                    accessMatrix.read(user, file)
                    accessMatrix.write(user, newFile)
                    logger.info("\'$user\' успешно скопировал \'$file\' в \'$directory\' ")
                    return newFile
                } else {
                    return null
                }
            }
        } else {
            return null
        }
    }



    //functions for TYPED MODEL
    fun addArcToTypeGraph(father:ModelType,daughter:ModelType):Boolean {
       /* //вариант защиты номер 1
        if (father == ModelType.ADMIN&&daughter == ModelType.NO_SECRET) {
            logger.severe("Ошибка в графе отношений! Попытка создания дуги от типа ADMIN к типу NO_SECRET")
            return false
        }*/
       //вариант защиты номер 2
        if (father == ModelType.NO_SECRET&&daughter == ModelType.SECRET) {
            logger.severe("Ошибка в графе отношений! Попытка создания дуги от типа NO_SECRET к типу SECRET")
            return false
        }
        typeGraph[father.id][daughter.id] = true
        return true
    }

    fun addArcsToTypeGraph(fathers:List<ModelType>, daughter: ModelType):Boolean {
        var result = true
        fathers.forEach { if (!addArcToTypeGraph(it,daughter)) result = false }
        return result
    }

    private fun initTypeGraph() {
        val n = ModelType.values.size
        for (i in 1..n) {
            val list = ArrayList<Boolean>(n)
            for (j in 1..n) {
                list.add(false)
            }
            typeGraph.add(list)
        }
    }



    //output functions
    fun printTypeGraph() {
        println("Граф отношений:")
        val n = typeGraph.size
        for (i in 0..n - 1) {
            val type = ModelType.values[i]

            val children = ModelType.values.filter { my_type -> typeGraph[i][my_type.id] }

            println("\t$type: $children")
        }
    }

    fun printState() {
        println()
        accessMatrix.printMatrix()
    }




}