import hru.ModelType
import hru.Object
import hru.Right
import model.Directory
import model.File
import model.User

var manager: Manager = Manager()

fun main(args: Array<String>) {
    val (admin, user) = createInitialState()
    manager.exploit(admin, user)
}

private fun createInitialState():Pair<User,User> {
    val s_1 = manager.createUser("s_1","admin", ModelType.ADMIN) as User
    val s_2 = manager.createUser("s_2","123456", ModelType.USER) as User

    val o_1 = manager.createDirectory(s_1,"o_1",ModelType.SECRET) as Directory
    val o_2 = manager.createDirectory(s_2,"o_2", ModelType.NO_SECRET) as Directory
    val o_3 = manager.createFile(s_1,"o_1","o_3",ModelType.SECRET) as File

    manager.giveRightFromOwner(s_2,s_1,o_2,Right.READ)
    manager.giveRightFromOwner(s_2,s_1,o_2,Right.WRITE)
    manager.giveRightFromOwner(s_2,s_1,o_2,Right.EXECUTE)
    manager.printState()

    return Pair(s_1,s_2)
}

fun Manager.exploit(s_1: User, s_2: User) {
    val o_1 = accessMatrix.findObject("o_1")
    val o_2 = accessMatrix.findObject("o_2")
    val o_3 = accessMatrix.findObject("o_3") as File

    val o_tr = exploit_step_1(s_1,s_2,o_2) as File

    val s_tr = exploit_step_2(s_1,o_1,o_2,o_3,o_tr) as User

    exploit_step_3(s_tr,o_3,o_2)

    println("Атака удалась! ХАХАХАХ")
    }

fun exploit_step_3(s_tr: User, o_3: Object, o_2: Object) {
    if (manager.checkRight(s_tr,o_3, Right.READ)&&manager.checkRight(s_tr,o_2, Right.WRITE)) {
        var o_I = manager.createFile(s_tr,o_2.name,"o'", ModelType.NO_SECRET) as File
        val fathers = listOf(ModelType.ADMIN, ModelType.SECRET, ModelType.NO_SECRET)
        if (manager.addArcsToTypeGraph(fathers, ModelType.SECRET)) {
            o_I = manager.copyFile(s_tr, o_3 as File,o_2.name,"o'") as File
            manager.accessMatrix.destroySubject(s_tr)
            manager.printState()
            manager.printTypeGraph()
        }
        else {
            System.err?.println("Атака не удалась на 3 шаге")
            System.exit(-1)
        }
    }
}

fun exploit_step_1(s_1: User, s_2: User, o_2: Object):File? {
    if (manager.checkRight(s_2,o_2, Right.WRITE)) {

        val o_tr = manager.createFile(s_2,o_2.name,"o_tr", ModelType.NO_SECRET) as File
        val fathers = listOf(ModelType.ADMIN, ModelType.USER, ModelType.NO_SECRET)
        if (manager.addArcsToTypeGraph(fathers, ModelType.NO_SECRET)) {
            if (manager.checkRight(s_1,o_2, Right.READ)&&manager.checkRight(s_1,o_2, Right.WRITE)) {
                manager.giveWRERights(s_1,o_tr)
                manager.printState()
                manager.printTypeGraph()
                return o_tr
            }
        }
        else {
            System.err?.println("Атака не удалась на шаге 1")
            System.exit(-1)
        }
    }
    else {
        return null
    }
    return null
}


fun exploit_step_2(s_1: User, o_1: Object, o_2: Object, o_3: Object, o_tr: Object):User? {
    if (manager.checkWRERights(s_1,o_tr)) {
        val s_tr = manager.createUser("s_tr","troyan", ModelType.ADMIN) as User
        val fathers = listOf(ModelType.ADMIN,ModelType.NO_SECRET,ModelType.USER, ModelType.SECRET)
        if (manager.addArcsToTypeGraph(fathers, ModelType.ADMIN)) {
            manager.giveWRERights(s_tr,o_2)
            if (manager.checkAllRights(s_1,o_1)&&manager.checkAllRights(s_1,o_3)) {
                manager.giveWRERights(s_tr,o_1)
                manager.giveWRERights(s_tr,o_3)

                manager.printState()
                manager.printTypeGraph()
                return s_tr
            }
        }
        else {
            System.err?.println("Атака не удалась на шаге 2")
            System.exit(-1)
        }
    }
    return null
}
