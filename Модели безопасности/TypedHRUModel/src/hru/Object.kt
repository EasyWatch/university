package hru

import java.io.Serializable

open class Object(var name:String,var type:ModelType = ModelType.NO_SECRET):Serializable {
    override fun toString(): String = "$name"
}

enum class ModelType(var id:Int) {
    ADMIN(0), USER(1), SECRET(2), NO_SECRET(3), NONE(4)
}
