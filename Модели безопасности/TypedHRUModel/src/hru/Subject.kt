package hru

import java.io.Serializable

open class Subject(name:String="", val password:String=""):Object(name),Serializable {

    override fun toString(): String = "$name"

    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Subject

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int = name.hashCode()

}