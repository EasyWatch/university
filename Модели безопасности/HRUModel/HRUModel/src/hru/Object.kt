package hru

import java.io.Serializable

open class Object(var name:String):Serializable {
    override fun toString(): String = "$name"
}