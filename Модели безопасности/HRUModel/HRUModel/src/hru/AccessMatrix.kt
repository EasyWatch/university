package hru

import java.io.Serializable
import java.util.*
import java.util.logging.FileHandler
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

class AccessMatrix:Serializable {
    val matrix: HashMap<Subject, HashMap<Object, HashSet<Right>>> = HashMap()
    val objectList: ArrayList<Object> = ArrayList()

    companion object {
        val logger: Logger = Logger.getLogger(this.toString())
    }

    fun findObject(objectName:String):Object {
        return objectList.find { o -> o.name.equals(objectName) } as Object
    }

        fun enter(r: Right, s: Subject, o: Object):Boolean {
            val subMatrix = matrix[s]
            if (subMatrix !=null ) {
               var setOfRights = subMatrix[o]
                if (setOfRights != null) {
                    setOfRights.add(r)
                    logger.info("Успешно добавлено право ${r.name} к субъекту \'$s\' на объект \'$o\'")
                    return true
                }
                else {
                    logger.severe("Ошибка! Невозможно добавить право ${r.name} к субъекту \'$s\' на объект \'$o\' так как данный объект не создан")
                    return false;
                }
            }
            else {
                logger.severe("Ошибка! Невозможно добавить право ${r.name} к субъекту \'$s\' на объект \'$o\' так как данный субъект не создан")
                return false;
            }
        }

    fun delete(r: Right, s: Subject, o: Object):Boolean {
        val subMatrix = matrix[s]
        if (subMatrix !=null ) {
            var setOfRights = subMatrix[o]
            if (setOfRights != null) {
                if (setOfRights.contains(r)) {
                    setOfRights.remove(r)
                    logger.info("Успешно удалено право ${r.name} у субъекта \'$s\' на объект \'$o\'")
                    return true
                }
                else {
                    logger.severe("Ошибка! Невозможно удалить право ${r.name} у субъекта \'$s\' на объект \'$o\', " +
                            "так как нет такого права у данного субъекта")
                    return false
                }
            }
            else {
                logger.severe("Ошибка! Невозможно удалить право ${r.name} у субъекта \'$s\' на объект \'$o\', так как не существует объект \'$o\' ")
                return false;
            }
        }
        else {
            logger.severe("Ошибка! Невозможно удалить право ${r.name} у субъекта \'$s\' на объект \'$o\'," +
                    " так как не существует субъект \'$s\' ")
            return false;
        }
    }

    fun createSubject(s: Subject):Boolean {
        if (matrix.containsKey(s)) {
            logger.severe("Ошибка! Субъект \'$s\' уже создан")
            return false
        }
        else {
            val subMatrix = HashMap<Object, HashSet<Right>>()
            objectList.forEach { o -> subMatrix.put(o, HashSet<Right>()) }
            matrix.put(s,subMatrix)
            logger.info("Успешно создан субъект \'$s\'")
            return true
        }
    }

    fun createObject(o: Object):Boolean {
        if (objectList.contains(o)) {
            logger.severe("Ошибка! Объект $o уже создан")
            return false
        }
        else {
            objectList.add(o)
            matrix.keys.forEach { s ->
                    matrix[s]?.put(o, HashSet<Right>())
            }
            logger.info("Успешно создан объект \'$o\'")
            return true
        }
    }

    fun destroySubject(s: Subject):Boolean {
        if (!matrix.containsKey(s)) {
            logger.severe("Ошибка! Субъект \'$s\' не может быть уничтожен, так как он не создан")
            return false
        }
        else {
            matrix.remove(s)
            logger.info("Успешно удален субъект \'$s\'")
            return true
        }
    }

    fun destroyObject(o: Object):Boolean {
        if (!objectList.contains(o)) {
            logger.severe("Ошибка! Объект \'$o\' не может быть уничтожен, так как он не создан")
            return false
        }
        else {
            matrix.keys.forEach { s-> matrix[s]?.remove(o) }
            logger.info("Успешно удален объект \'$o\'")
            return true
        }
    }

    fun checkRight(r: Right, s: Subject, o: Object):Boolean = matrix[s]?.get(o)?.contains(r) as Boolean

    fun printMatrix():Unit {
        matrix.keys.forEach { s -> println("$s")
        matrix[s]?.forEach {o, hashSet -> println("\t$o: $hashSet")} }
    }



    fun read(s: Subject, o: Object):Boolean {
        if (checkRight(Right.READ,s,o)) {
            logger.info("\'$s\' успешно прочитал \'$o\'")
            return true
        }
        else {
            logger.severe("Ошибка! \'$s\' не имеет прав чтения \'$o\'")
            return false
        }
    }

    fun write(s: Subject, o: Object):Boolean {
        if (checkRight(Right.WRITE,s,o)) {
            logger.info("\'$s\' успешно записал в \'$o\'")
            return true
        }
        else {
            logger.severe("Ошибка! \'$s\' не имеет прав записи \'$o\'")
            return false
        }
    }

    init {
        logger.useParentHandlers = false
        val handler = FileHandler("src/hru/hru_logs",true)
        val formatter = SimpleFormatter()
        handler.formatter = formatter;
        logger.addHandler(handler)
    }
}