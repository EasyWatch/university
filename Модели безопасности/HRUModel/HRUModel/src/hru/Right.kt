package hru

enum class Right(type: String) {
    WRITE("write"),
    READ("read"),
    OWN("own"),
    EXECUTE("execute")
}