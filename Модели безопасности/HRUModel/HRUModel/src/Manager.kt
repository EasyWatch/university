import hru.AccessMatrix
import hru.Object
import hru.Right
import model.Directory
import model.File
import model.User
import java.io.Serializable
import java.util.*
import java.util.logging.FileHandler
import java.util.logging.Logger
import java.util.logging.SimpleFormatter

class Manager : Serializable {

    val accessMatrix: AccessMatrix = AccessMatrix()

    val directories: HashMap<String, Directory> = HashMap()

    val admins: HashSet<User> = HashSet()

    val users: HashSet<User> = HashSet()

    companion object {
        var logger = Logger.getLogger(this.toString())
    }

    init {
        logger.useParentHandlers = false
        val handler = FileHandler("manager_logs",true)
        val formatter = SimpleFormatter()
        handler.formatter = formatter;
        logger.addHandler(handler)
    }

    //функции создания объектов и субъектов
    fun createUser(name: String, password: String): User? {
        val user = User(name, password)

        if (accessMatrix.createSubject(user)) {
            logger.info("Успешно создан \'${user}\'")
            users.add(user)
            return user
        }
        else {
            logger.severe("Ошибка! создание \'${user}\' провалилось")
            return null
        }
    }

    fun createAdmin(name: String, password: String): User? {
        val admin = User(name, password, true)

        if (accessMatrix.createSubject(admin)) {
            accessMatrix.objectList.forEach { o -> giveWRERight(admin, o) }
            admins.add(admin)
            users.add(admin)
            logger.info("Успешно создан \'${admin}\'")
            return admin
        }
        else {
            logger.severe("Ошибка! Создание \'${admin} провалилось")
            return null
        }
    }

    fun createFile(user: User, directoryName: String, fileName: String): File? {
        val file = File(fileName,user.isAdmin)
        val directory = directories[directoryName]

        if (directory != null) {
            if (directory.isAdminable) {
                if (!user.isAdmin) {
                    logger.severe("Ошибка! Попытка создания в папки администратора не администратором")
                    return null
                }
            }

            if (accessMatrix.checkRight(Right.WRITE, user, o = directory)) {
                accessMatrix.createObject(file)
                accessMatrix.enter(Right.OWN, user, file)
                giveWRERight(user, file)
                directory.files += file
                admins.forEach { admin -> giveWRERight(admin, file) }
                logger.info("Успешно создан пользователем \'$user\' файл \'$fileName\' в директории \'$directoryName\'")
                return file
            }
            else {
                logger.severe("Ошибка! У \'$user\' нет права записи в \'$directory\'")
            }
        }
        return null
    }

    fun createDirectory(user: User, directoryName: String): Directory {
        val directory = Directory(directoryName)
        accessMatrix.createObject(directory)
        directories.put(directoryName,directory)
        accessMatrix.enter(Right.OWN, user, directory)
        giveWRERight(user, directory)
        admins.forEach { admin -> giveWRERight(admin, directory) }
        logger.info("\'$user\' успешно создал \'$directory\'")
        return directory
    }


    //функции  работы с правами
    fun giveWRERight(user: User, o: Object) {
        accessMatrix.enter(Right.READ, user, o)
        accessMatrix.enter(Right.WRITE, user, o)
        accessMatrix.enter(Right.EXECUTE, user, o)
    }

    fun checkWRERight(user:User, o:Object):Boolean {
        return (accessMatrix.checkRight(Right.READ, user, o))&&
                (accessMatrix.checkRight(Right.WRITE, user, o))&&
                (accessMatrix.checkRight(Right.EXECUTE, user, o))
    }

    fun giveRightFromOwner(owner:User,user:User,file:File,right: Right) {
        if (accessMatrix.checkRight(Right.OWN,owner,file)&&right != Right.OWN) {
            accessMatrix.enter(right,user,file)
        }
    }


    fun copyFile(user: User,file:File,directoryName: String,newFileName:String):File? {
        val directory = directories[directoryName]

        if (directory != null) {
            if ((file.isAdminable&&!user.isAdmin)) {
                logger.severe("Ошибка! Попытка не администратора \'$user\' скопировать администраторский \'$file\' не удалась")
                return null
            }
            else {
                if (accessMatrix.checkRight(Right.READ,user,file)) {
                    val newFile = createFile(user,directoryName,newFileName) as File
                    accessMatrix.read(user,file)
                    accessMatrix.write(user,newFile)
                    logger.info("\'$user\' успешно скопировал \'$file\' в \'$directory\' ")
                    return newFile
                }
                else {
                    return null
                }
            }
        }
        else {
            return null
        }
    }

    fun printState() {
        accessMatrix.printMatrix()
    }


}