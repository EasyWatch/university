import hru.Right
import model.File
import model.User

var manager: Manager = Manager()

fun main(args: Array<String>) {
    val (admin, user) = createInitialState1()
    manager.exploit1(admin, user)
}

fun Manager.exploit2(s_1: User,s_2:User) {
    val o_1 = accessMatrix.findObject("o_1")
    val o_2 = accessMatrix.findObject("o_2") as File

    println("Атака")
    println("Шаг №1")
    giveRightFromOwner(s_2,s_1,o_2, Right.WRITE)
    giveRightFromOwner(s_2,s_1,o_2, Right.EXECUTE)
    printState()
    this.exploit1(s_1,s_2)
}

fun Manager.exploit1(s_1: User, s_2: User) {
    val o_1 = accessMatrix.findObject("o_1")
    val o_2 = accessMatrix.findObject("o_2")
    val o_3 = accessMatrix.findObject("o_3") as File

    println("\nАтака")
    println("Шаг №1")
    val troyanFile = this.createFile(s_2, "o_2", "o_tr") as model.File
    printState()

    println("\nШаг №2")
    if (checkWRERight(s_2, troyanFile)) {
        val user_tr = this.createUser("ТРОЯНСКИЙ", "троянский") as User
        giveWRERight(user_tr, o_2)
        giveWRERight(user_tr, troyanFile)
        if (checkWRERight(s_1, o_1) && accessMatrix.checkRight(Right.OWN, s_1, o_1) &&
                (checkWRERight(s_1, o_3) && accessMatrix.checkRight(Right.OWN, s_1, o_3))) {
            giveWRERight(user_tr, o_1)
            giveWRERight(user_tr, o_3)
        }
        printState()

        println("\nШаг №3")
        if (accessMatrix.checkRight(Right.READ, user_tr, o_3) && accessMatrix.checkRight(Right.WRITE, user_tr, o_2)) {
            val copy_o_3 = copyFile(user_tr,o_3,"o_2","copy_o_3")
            if (copy_o_3 != null) {
                accessMatrix.enter(Right.READ, s_2, copy_o_3)
                accessMatrix.destroySubject(user_tr)
            }
            else {
                System.err?.println("Атака не удалась")
            }
        }
        printState()
    }

}

private fun createInitialState2():Pair<User,User> {
    val s_1 = manager.createAdmin("s_1","admin12345") as User
    val s_2 = manager.createUser("s_2","макака") as User
    val o_1 = manager.createDirectory(s_1,"o_1")
    val o_2 = manager.createDirectory(s_2,"o_2")
    val o_3 = manager.createFile(s_1,"o_1","o_3")
    manager.accessMatrix.delete(Right.WRITE,s_1,o_2)
    manager.accessMatrix.delete(Right.EXECUTE,s_1,o_2)
    manager.printState()

    return Pair(s_1,s_2)
}

private fun createInitialState1(): Pair<User, User> {
    val admin = manager.createAdmin("БОГ", "admin12345") as User
    val usualUser = manager.createUser("calmarj", "monkey") as User
    manager.createDirectory(admin, directoryName = "o_1")
    manager.createDirectory(usualUser, "o_2")
    manager.createFile(admin, "o_1", "o_3")
    return Pair(admin, usualUser)
}
