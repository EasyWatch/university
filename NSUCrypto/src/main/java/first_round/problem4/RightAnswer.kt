package first_round.problem4

import java.io.File


fun main(args: Array<String>) {
    val lines = File("/Users/calmarj/Университет/NSUCrypto/src/first_round.problem4/input").readLines()
    for (line in lines) {

        println(replaceForRoman(line).toUpperCase())
    }
    println(replaceForStepah(lines[0]))
}

fun replaceForStepah(s: String): String {
    var result = s.replace("4"," ")
    result = result.replace("F","i")
    result = result.replace("V","s")
    result = result.replace("A","a")
    result = result.replace("Q","w")
    result = result.replace("?","t")
    result = result.replace("2","h")
   result = result.replace("J","d")
    result = result.replace("S","v")
    result = result.replace("K","e")
    result = result.replace("G","y")
    result = result.replace("C","u")
    result = result.replace("X","r")
    result = result.replace("I","f")
    result = result.replace("O","o")


    return  result
}

fun replaceForRoman(line: String): String {
    var result = line.replace("4"," ")
    result = result.replace("2","t")
    result = result.replace("?","h")
    result = result.replace("K","e")
    result = result.replace("J","d")
    result = result.replace("X","r")
    result = result.replace("L","u")
    result = result.replace("7","y")
    result = result.replace("Q","w")
    result = result.replace("A","a")
    result = result.replace("O","o")
    result = result.replace("N","n")
    result = result.replace("M","m")
    result = result.replace("S","s")
    result = result.replace("U","l")

    return result
}
