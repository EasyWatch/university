package problem1


import java.util.*

/**
 * Created by calmarj on 15.11.15.
 */

val ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

fun main(args: Array<String>) {
    /*val cipher = problem1.getCipher()

    cipher.forEach { println(it) }
    println()
    val scanner = Scanner(File("problem1_input.txt"))

    while(scanner.hasNextLine()) {
        val line = scanner.nextLine().split(" ")
        for (letter in line) {
            val indexes = letter.replace("(","").replace(")","").split(",")
            val i = Integer.parseInt(indexes[0])
            val j = Integer.parseInt(indexes[1])
            print(cipher[i][j])
        }
        print(" ")
    }

   // problem1.getFragment()*/

    //findWord()

}

fun getCipher(): List<String> {
    val result = ArrayList<String>()
    result.add("")
    result.add(1, getRow('P', 8))
    result.add(2, getRow('T', 7))
    result.add(3, getRow('S'))
    result.add(4, getRow('P'))
    result.add(5, getRow('I'))
    result.add(6, getRow('T', 3))
    result.add(7, getRow('E', 5))
    result.add(8, getRow('T', 1))
    result.add(9, getRow('I'))
    result.add(10, getRow('O'))
    result.add(11, getRow('N'))

    return result
}

fun getRow(c: Char,index:Int=1): String {
    var result = " "
    var letter = c - index+1
    for (i in 1..11) {
        result+=letter
        if (letter!='*') {
            if (letter=='Z') {
                letter = 'A'
            }
            else {
                letter += 1
            }
        }
    }
    return result
}

private fun getFragment() {
    val row1 = fixRow('M')
    val row2 = fixRow('S')
    val row3 = fixRow('R')
    println(row1)
    println(row2)
    println(row3)
}

fun fixRow(c: Char, index:Int=1): String {
    var result = " "
    var letter = c
    if (c != '*') {
        letter= c - index + 1
    }
    for (i in 1..11) {
        result=letter+" "+result
        if (letter!='*') {
            if (letter=='Z') {
                letter = 'A'
            }
            else {
                letter -= 1
            }
        }
    }
    return result
}


