package first_round.problem3

/**
 * Created by calmarj on 15.11.15.
 */

fun main(args: Array<String>) {
    for (x in 1..100000)
        if (eq(x)) {
            println(x)
        }

    val b = 56
    val c = 6

    val d = -4*b*b*b-27*c*c
    println(d)

}

fun eq(x: Int): Boolean {
    val leftPart = x*x*x+56*x+6
    val doubleLeft = leftPart+0.0
    val round = Math.round(Math.sqrt(doubleLeft))
    if (Math.abs((round * round)-(leftPart)) <0.001) {
        return true
    }
    return false
}
