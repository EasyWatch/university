import java.util.ArrayList

/**
 * Created by calmarj on 16.11.15.
 */
object Utils {
    public fun splitEqually(text: String, size: Int): List<String> {
        // Give the list the right capacity to start with. You could use an array
        // instead if you wanted.
        val ret = ArrayList<String>((text.length + size - 1) / size)

        var start = 0
        while (start < text.length) {
            ret.add(text.substring(start, Math.min(text.length, start + size)))
            start += size
        }
        return ret

    }
}
