package second_round.problem9

import java.io.File
import java.util.*

/**
 * Created by calmarj on 18.11.15.
 */

fun main(args: Array<String>) {
    val scanner = Scanner(File("/Users/calmarj/Университет/NSUCrypto/src/main/java/second_round/problem9/input_problem9"))
    val n = scanner.nextInt()
    scanner.nextLine()
    val matrix = ArrayList<Vector>()
    for (i in 1..n/2) {
        val elements = scanner.nextLine().split(" ")
        val list = ArrayList<Boolean>()
        elements.forEach { list.add(it.equals("1")) }
        matrix.add(Vector(n,list))
    }

    val radius = findCoveringRadius(n,matrix)
    println(radius)

}

fun findCoveringRadius(n: Int, matrix: ArrayList<Vector>): Int {
    val upper_bound = Math.pow(2.0,n+0.0).toInt()
    var max_distance = 0
    var max_vector = Vector()
    for (i in 0..upper_bound-1) {
        val binary_s = getBinaryS(i,n)
        val vector = Vector(binary_s)
        val distance = vector.getDistance(matrix)
        if (distance>max_distance) {
            max_distance = distance
            max_vector = vector
        }
    }
    println(max_vector)
    return max_distance
}

fun getBinaryS(i: Int, length: Int): String {
    var s = Integer.toBinaryString(i)
    var zeroesCount = length - s.length
    for (j in 1..zeroesCount) {
        s = "0"+s
    }
    return s
}


