package second_round.problem9

import java.util.*


data class Vector(var n:Int=0,val elements: ArrayList<Boolean> = ArrayList<Boolean>()) {

    constructor(s:String) : this() {
        n = s.length
        for (letter in s) {
            elements.add(letter.equals('1'))
        }
    }

    fun getDistance(anotherVector: Vector):Int {
        //if (anotherVector.n != this.n) return null
        var distance = 0
        for (i in 0..n-1) {
            if (anotherVector.elements[i] != this.elements[i]) {
                distance+=1
            }
        }
        return distance
    }

    fun getDistance(subSpace:ArrayList<Vector>):Int {
        var distance = n
        subSpace.forEach {
            vector ->
            var vector_distance = getDistance(vector)
            if (vector_distance<distance) distance = vector_distance
        }
        return distance
    }
}