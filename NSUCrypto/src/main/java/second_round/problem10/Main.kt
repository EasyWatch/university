package second_round.problem10

import java.io.File
import Utils
import java.util.*

/**
 * Created by calmarj on 16.11.15.
 */

fun main(args: Array<String>) {
    val input = File("/Users/calmarj/Университет/NSUCrypto/src/second_round/problem10/problem_4_input").readText()
    val line = input.replace(" ","").replace("\"","").replace("-","")
    val tokens = Utils.splitEqually(line,2)
    val bigrams = ArrayList<Bigram>()

    for (token in tokens) {
        val bigram = Bigram(token[0], token[1])
        bigrams.add(bigram)
    }

    val map = bigrams.groupBy { it -> it.type  }.toLinkedMap()

    println()

}

