package second_round.problem10

import VOWELS

/**
 * Created by calmarj on 16.11.15.
 */
class Bigram(val firstLetter:Char,val secondLetter:Char) {
    public val type = getType(firstLetter,secondLetter)

    private fun getType(firstLetter: Char, secondLetter: Char): BigramType {
        if (isVowel(firstLetter)&&isVowel(secondLetter)) {
            return BigramType.VOWELS
        }
        else if (isVowel(firstLetter)||isVowel(secondLetter)) {
            return BigramType.MIXED
        }
        else {
            return BigramType.CONSONANTS
        }
    }

    private fun isVowel(letter:Char):Boolean {
        return VOWELS.contains(letter)
    }



}

fun main(args: Array<String>) {
    //println(Bigram())
}

enum class BigramType {
    VOWELS, CONSONANTS, MIXED
}