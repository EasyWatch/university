package second_round.problem4

import org.apache.http.client.HttpClient
import java.io.File
import java.util.*
import Utils

/**
 * Created by calmarj on 17.11.15.
 */
fun main(args: Array<String>) {
    val map = HashMap<Char, HashMap<String,Int>>()
    val lines = File("/Users/calmarj/Университет/NSUCrypto/src/main/java/second_round/problem4/log.txt").readLines()
    for (line in lines) {
        if ((!(line.startsWith("Navigated"))) && line.trim().length != 0) {
            val tokens = line.split(" ")
            val plaintext = tokens[0]
            val ciphertext = tokens[1]
            val letter = plaintext[0]
            map.putIfAbsent(letter,HashMap<String,Int>())

            val bigrams = Utils.splitEqually(ciphertext,2)
            for (bigram in bigrams) {
                val sub_map = map.get(letter) as HashMap<String,Int>
                sub_map.putIfAbsent(bigram,0)
                var counter = sub_map.get(bigram) as Int
                sub_map.put(bigram,(counter+1))
            }
        }
    }

    map.forEach {
        letter, map ->
        print("$letter("+map.size+"):")

        map.forEach { bigram, count -> print("$bigram($count)") }
        println()
        println()
    }

   /* map.keys.forEach {
        val set = map.getOrDefault(it,HashSet())
        for (key in map.keys) {
            if (key != it) {
                val key_set = map.getOrDefault(key, HashSet())
                val sub_set = set.subtract(key_set)
                if (sub_set.size<set.size) {
                    println(it+" "+key)
                }
            }
        }
    }
*/
    /*println()
    map.keys.forEach {
        print("$it")
        val count = map.get(it)?.count { bigram -> bigram.contains(it) }
        println(" - $count")
    }*/
   println()
    print("NOPE :")
    for (first_letter in 'A'..'Z') {
        for (second_letter in 'A'..'Z') {
            val buffer = StringBuffer()
            buffer.append(first_letter)
            buffer.append(second_letter)
            val s = buffer.toString()

            if (!(findBigram(s,map))) {
                print(s+ " ")
            }

        }
    }
    println()
    val cipher = "NBYSUDOJOWUQPULBYHCAHRUDKZJTQPELLXQWJOAJHCJAZTCEBFKKJNHPMZAGGNPFOJHCKHVKIPUDBETUITSMSK"
    println("Cipher: $cipher")
    val bigrams = Utils.splitEqually(cipher,2)
    var message = ""
    bigrams.forEach {
        message+=(findBigramInMap(it,map))
    }
    println("Messag: $message")


}

fun findBigramInMap(letter: String, map: HashMap<Char, HashMap<String, Int>>):Char {
    var result:Char='1'
    map.keys.forEach {
        if (map[it]?.keys?.containsRaw(letter) as Boolean) {
            result = it
        }
    }
    return result
}

fun findBigram(s: String, map: HashMap<Char, HashMap<String,Int>>):Boolean{
    var found = false
    map.keys.forEach {
        if (map.get(it)?.keys?.contains(s) as Boolean) {
            found = true
        }
    }
    return found

}
