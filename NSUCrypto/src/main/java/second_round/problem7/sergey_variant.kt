package second_round.problem7

/**
 * Created by calmarj on 20.11.15.
 */
import java.lang.Byte
import Utils
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec

fun encrypt(des_key:String,blow_key:String,message:String):String {
    var desCipher: Cipher
    desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

    val ascii_block_des = transform_string_block_to_int(des_key)
    val byte_block_des = transform_to_bytes(ascii_block_des)

    var desKey = byte_block_des //3837363534333231
    var KS: SecretKeySpec = SecretKeySpec(desKey, "DES");
    desCipher.init(Cipher.ENCRYPT_MODE, KS);
    var text = transform_from_string_bytes(message)
    var textEncrypted = desCipher.doFinal(text);

    pt(textEncrypted.copyOfRange(0, 8))
    var blowText = textEncrypted.copyOfRange(0, 8) //cb32b921efe674e5
    var blowCipher: Cipher
    blowCipher = Cipher.getInstance("Blowfish/ECB/PKCS5Padding");
    var second = blow_key
    val ascii_block_blow = transform_string_block_to_int(second)
    val byte_block_blow = transform_to_bytes(ascii_block_blow)
    //var blowKey = byteArrayOf(0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38) //3132333435363738
    var blowKey = byte_block_blow //3132333435363738
    var bKS: SecretKeySpec = SecretKeySpec(blowKey, "Blowfish");
    blowCipher.init(Cipher.ENCRYPT_MODE, bKS);
    var blowTextEncrypted = blowCipher.doFinal(blowText);
    return pt(blowTextEncrypted.copyOfRange(0, 8))
}

fun pt(ba: ByteArray):String {
    var result = ""
    for (b in ba) {
        result+=(javax.xml.bind.DatatypeConverter.printHexBinary(byteArrayOf(b)))
    }
    return result
}

fun transform_from_string_bytes(s:String):ByteArray {
    return transform_to_bytes(transform_string_block_to_int(s))
}
fun transform_from_bytes(array:ByteArray):String {
    var result = ""
    for (byte in array) {
        result+=Integer.toHexString(Integer.parseInt(Byte.toString(byte)))
    }
    return result
}

fun transform_string_block_to_int(messageblock: String):String {
    var result = ""

    for (letter in messageblock) {
        val ascii_letter = letter.toInt()
        result=Integer.toHexString(ascii_letter)+result
    }
    return result
}

fun transform_to_bytes(deskey: String): ByteArray {
    val bytes_string=Utils.splitEqually(deskey,2)
    val bytes = ByteArray(bytes_string.size)
    var i = 0
    bytes_string.forEach {
        bytes[i] = Byte.parseByte(it,16)
        i++
    }
    return bytes
}

