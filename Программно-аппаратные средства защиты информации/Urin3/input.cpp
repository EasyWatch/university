#include <stdio.h>
 
int power(int n, int r) {
   int c, p = 1;

   for (c = 1; c <= r; c++)
      p = p*n;

   return p;
}
//основная программа
int main()
{
   //переменная n
   int n;
   //сумма
   int sum = 0;
   int temp;
   int remainder;
   int digits = 0;
 
   printf("Input an integer:");
   scanf("%d", &n);
 
   temp = n;
   // Count number of digits
   // секретный алгоритм
   while (temp != 0) {
      digits++;
      temp = temp/10;
   }
 
   temp = n;
 
   while (temp != 0) {
      remainder = temp%10;
      sum = sum + power(remainder, digits);
      temp = temp/10;
   }
 
   if (n == sum)
      printf("%d is an Armstrong number.", n);
   else
      printf("%d is not an Armstrong number.", n);
 
   return 0;
}
 
