#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

from random import randint

FUNCTION_NAME_LENGTH = 15
VARIABLE_NAME_LENGTH = 10
ID_LENGTH = 10

GARBAGE_MAX_FUNCTIONS = 5
JUNK_MAX_PARAMS_PER_FUNCTION = 5
JUNK_MAX_INSTRUCTIONS = 10

non_start_text = "(?!(?:\/\/|\"|\'|\/\*\*))"
comments = "(\/\/.*\n|\/\*\s*\n*\s*(?:.*)\s*\n*\s*\*\/)"
identifier = "[a-zA-Z][\w|_]*"
declarator = "(?:\*?)\s*" + "(" + identifier + ")"
storage_class_specifiers = "register|static|extern|typedef"
type_qualifiers = "const|volatile"
type_specifiers = "void|char|short|int|long|float|double|struct|union"
storage_class_specifiers_functions = "static|extern"
type_qualifiers_functions = type_qualifiers
type_specifiers_functions = "char|short|int|long|float|double|signed|unsigned"
my_type_specifiers = "int|long"
declaration_specifiers = "(?:" + storage_class_specifiers + ")?\s*(?:" + type_qualifiers + ")?\s*(?:" + type_specifiers + ")\*?\s+"
function_definition = non_start_text + declaration_specifiers + declarator + "\s*\("
start_preprocessor = "(?#)"
non_start_preprocessor = "(?!#)"
preprocessor_defines = "define|ifdef|ifndef|undef|include"
preprocessor = non_start_text + start_preprocessor + "\s*(?:" + preprocessor_defines + ")\s*" + declarator + "\s*\(?"
variable_definition = non_start_text + declaration_specifiers + declarator + "\s*(?:[=|;|,),])"
separated_variables = non_start_text + identifier + "\s*,\s*" + declarator + "\s*(?:[=|;|,])"
token_numbers = non_start_text + "((?:0x|0b|0x%)?[0-9]+)"


def read_source(fileName):
    result = ""
    with open(fileName, "r") as fd: result += fd.read()
    fd.close()
    return result


def write_source(source, fileName):
    with open(fileName, "w") as fd: fd.write(source)
    fd.close()


def remove_comments(source):
    source = re.sub(comments, "", source)
    return source


def obfuscate_numbers(number):
    converters = [lambda x:bin(int(x, 0)), lambda x:hex(int(x, 0)), lambda x:oct(int(x, 0))]
    delta = randint(0, 1000)
    if randint(0, 100) % 2 == 0:
        number = str(int(number) + delta)
        return "(" + converters[randint(0, len(converters) - 1)](number) + "-" + converters[
            randint(0, len(converters) - 1)](str(delta)) + ")"
    else:
        number = str(int(number) - delta)
        return "(" + converters[randint(0, len(converters) - 1)](number) + "+" + converters[
            randint(0, len(converters) - 1)](str(delta)) + ")"


def rename_functions(source, functions):
    for function in functions: source = re.sub(r'\b' + function + r'\b', functions[function], source)
    return source


def replace_declarations(source, declarations):
    for declaration in declarations: source = re.sub(r'\b' + declaration + r'\b', declarations[declaration], source)
    return source


def rename_variables(source, variables):
    for variable in variables: source = re.sub(r'\b' + variable + r'\b', variables[variable], source)
    return source


def obfsuc_numbers(source, numbers):
    for number in numbers: source = re.sub(r'\b' + number + r'\b', numbers[number], source)
    return source


def find_functions(source, id_length):
    try:
        functions = {}
        reCompiled = re.compile(function_definition)
        for match in reCompiled.finditer(source):
            funcToken = match.group(1)
            if funcToken != "main":
                if funcToken not in functions: functions[funcToken] = generate_valid_id(id_length)
    except AttributeError as ae:
        print "AttributeError"
    except IndexError as ie:
        print "Index error"
    finally:
        return functions


def search_declaration(source, id_length):
    try:
        declarations = {}
        reCompiled = re.compile(preprocessor)
        for match in reCompiled.finditer(source):
            decToken = match.group(1)
            if decToken not in declarations: declarations[decToken] = generate_valid_id(id_length)
    except AttributeError as ae:
        print "AttributeError"
    except IndexError as ie:
        print "Index error"
    finally:
        return declarations


def find_variables(source, id_length):
    try:
        variables = {}
        reCompiled = re.compile(variable_definition)
        for match in reCompiled.finditer(source):
            varToken = match.group(1)
            if varToken not in variables: variables[varToken] = generate_valid_id(id_length)
    except AttributeError as ae:
        print "AttributeError"
    except IndexError as ie:
        print "Index error"
    finally:
        return variables


def find_numbers(source):
    try:
        numbers = {}
        reCompiled = re.compile(token_numbers)
        for match in reCompiled.finditer(source):
            numToken = match.group(1)
            if numToken not in numbers:
                numbers[numToken] = obfuscate_numbers(numToken)
    except AttributeError as ae:
        print "AttributeError"
    except IndexError as ie:
        print "Index error"
    finally:
        return numbers


def generate_valid_id(length):
    return "".join([chr(randint(65, 90)) if randint(0, 2) >= 1 else chr(randint(97, 122))] + [
        chr(randint(65, 90)) if randint(0, 2) == 2 else chr(randint(97, 122)) if randint(0, 2) == 1 else chr(
            randint(48, 57)) for x in xrange(length)])


def generate_function(num_params):
    astorage_specifiers = storage_class_specifiers_functions.split("|")
    astorage_specifiers.append("")
    atype_qualifiers = type_qualifiers_functions.split("|")
    atype_qualifiers.append("")
    atype_specifiers = type_specifiers_functions.split("|")
    my_storage_specifier = astorage_specifiers[randint(0, len(astorage_specifiers) - 1)]
    my_type_qualifier = atype_qualifiers[randint(0, len(atype_qualifiers) - 1)]
    my_type_specifier = atype_specifiers[randint(0, len(atype_specifiers) - 1)]
    len_name = randint(3, ID_LENGTH)
    my_name = generate_valid_id(len_name)
    source_function = my_storage_specifier + " " + my_type_qualifier + " " + my_type_specifier + " " + my_name + "("
    source_function += atype_specifiers[randint(0, len(atype_specifiers) - 1)] + " " + generate_valid_id(
        randint(0, ID_LENGTH))
    for x in xrange(num_params - 1):
        source_function += "," + atype_specifiers[randint(0, len(atype_specifiers) - 1)] + " " + generate_valid_id(
            randint(0, ID_LENGTH))
    source_function += "){}"
    return source_function


def generate_hard_function():
    astorage_specifiers = storage_class_specifiers_functions.split("|")
    astorage_specifiers.append("")
    atype_qualifiers = type_qualifiers_functions.split("|")
    atype_qualifiers.append("")
    atype_specifiers = type_specifiers_functions.split("|")
    my_storage_specifier = astorage_specifiers[randint(0, len(astorage_specifiers) - 1)]
    my_type_qualifier = atype_qualifiers[randint(0, len(atype_qualifiers) - 1)]
    my_type_specifier = atype_specifiers[randint(0, len(atype_specifiers) - 1)]
    len_name = randint(3, ID_LENGTH)
    my_name = generate_valid_id(len_name)
    source_function = my_storage_specifier + " " + my_type_qualifier + " " + my_type_specifier + " " + my_name + "() {}"
    blank_function = source_function
    var_num = randint(1, 10)
    declarations = []
    for i in xrange(var_num):
        declaration = generate_declaration()
        declarations.append(declaration)
        var_type, var_name, dec_string = declaration
        blank_function = blank_function[
                         :blank_function.__len__() - 1] + dec_string + "\n}"
    inner_function_name, inner_function = generate_function_dec(declarations)
    blank_function = inner_function + "\n" + blank_function
    function_call = inner_function_name + "("
    for declaration in declarations:
        function_call += declaration[1] + ","
    function_call = function_call[:function_call.__len__() - 1] + ");"
    blank_function = blank_function[:blank_function.__len__() - 1] + "\t"+function_call + "}"

    return blank_function,my_name


def generate_function_dec(declarations):
    astorage_specifiers = storage_class_specifiers_functions.split("|")
    astorage_specifiers.append("")
    atype_qualifiers = type_qualifiers_functions.split("|")
    atype_qualifiers.append("")
    atype_specifiers = type_specifiers_functions.split("|")
    my_storage_specifier = astorage_specifiers[randint(0, len(astorage_specifiers) - 1)]
    my_type_qualifier = atype_qualifiers[randint(0, len(atype_qualifiers) - 1)]
    my_type_specifier = atype_specifiers[randint(0, len(atype_specifiers) - 1)]
    len_name = randint(3, ID_LENGTH)
    my_name = generate_valid_id(len_name)
    num_params = randint(1, JUNK_MAX_PARAMS_PER_FUNCTION)
    source_function = my_storage_specifier + " " + my_type_qualifier + " " + my_type_specifier + " " + my_name + "("
    params = []
    for dec in declarations:
        type_var, name_var, dec_string = dec
        source_function += type_var + " " + name_var + ", "
    source_function = source_function[:source_function.__len__() - 2] + "){}"
    return my_name, source_function


def generate_declaration():
    atype_specifiers = my_type_specifiers.split("|")
    my_type_specifier = atype_specifiers[randint(0, len(atype_specifiers) - 1)]
    value = (str(randint(1, 256)))
    value = hex(int(value, 0))
    my_name = generate_valid_id(VARIABLE_NAME_LENGTH)
    declaration = "\t" + my_type_specifier + " " + my_name + " = " + value + ";"
    return my_type_specifier, my_name, declaration


def put_junk_function(source):
    garbage_func,function_name = generate_hard_function()
    index = source.find("main")
    times = randint(5,10)
    for i in xrange(times):
        index = source.find("\n",index+1)
    source = source[:index]+function_name+"();"+source[index:]
    index = source.find(functions.keys()[0])
    index = source.rfind("\n", 0, index)
    source = source[:index] + "\n\n" + garbage_func + "\n" + source[index:]

    return source


if __name__ == "__main__":
    source = read_source(raw_input("Введите название файла для обфускации:"))
    # удаление комментариев
    source = remove_comments(source)
    # замена чисел на выражения
    numbers = find_numbers(source)
    source = obfsuc_numbers(source, numbers)

    functions = find_functions(source, FUNCTION_NAME_LENGTH)
    # добавление сложной функции мусора
    source = put_junk_function(source)
    # переименование названий переменных
    source = rename_functions(source, functions)

    # переименование переменных
    variables = find_variables(source, VARIABLE_NAME_LENGTH)
    source = rename_variables(source, variables)
    # сохранение результата
    write_source(source, "output.cpp")
    print "Процесс обфускации успешно завершен,  сохранено  output.cpp!"
