package task3.part2;

import task3.Substitution;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by calmarj on 08.03.16.
 */
public class Part2 {

    public static final String ALPHABET = "abcd";

    private static final ArrayList<Substitution> substitutions = getInput();

    public static void main(String[] args) throws FileNotFoundException {

       substitute("c","c");
    }

    private static void substitute(String axiom,String history) {
        for (Substitution substitution : substitutions) {
            if (axiom.contains(substitution.getFrom())) {
                String newAxiom = substitution.makeSubs(axiom);
                String newHistory = history+"<-"+newAxiom;
                System.out.println(newHistory);
                if (newAxiom.length()<6) {
                    substitute(newAxiom, newHistory);
                }
            }
        }
    }


    private static ArrayList<Substitution> getInput() {
        Scanner scanner = null;
        try {

            scanner = new Scanner(new FileInputStream(new File("/Users/calmarj/Университет/university/Mironov/src/task3/part2/part2_input.txt")));
            ArrayList<Substitution> subs = new ArrayList<>();

            while (scanner.hasNextLine()) {
                subs.add(new Substitution(scanner.nextLine()));
            }
            return subs;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static void iterate(char[] chars, int len, char[] build, int pos) {
        if (pos == len) {
            String word = new String(build);
            // do what you need with each word here
            return;
        }

        for (char letter : chars) {
            build[pos] = letter;
            iterate(chars, len, build, pos + 1);
        }
    }

}
