package task3;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Substitution {

    private String from;

    private String to;

    private int count;

    public Substitution(String line) {


        String []tokens = line.split("->");
        this.from = tokens[0].trim();
        this.to = tokens[1].trim();
        if (to.equals("e")) to = "";
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public boolean canBeUsed() {
        return count!=0;
    }

    public String makeSubs(String axiom) {
        count--;
        return axiom.replace(from,to);
    }
}
