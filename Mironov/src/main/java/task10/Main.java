package task10;

import com.mifmif.common.regex.Generex;
import com.mifmif.common.regex.util.Iterator;

import java.util.List;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Main {
    public static void main(String[] args) {
        Generex generex = new Generex("{a}*{b}{a}*{b}{a}*");

        // Generate random String
        String randomStr = generex.random();
        System.out.println(randomStr);// a random value from the previous String list

        // Generate all String that matches the given Regex.
        List<String> matchedStrs = generex.getAllMatchedStrings();

        // Using Generex iterator
        Iterator iterator = generex.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
    }
}
