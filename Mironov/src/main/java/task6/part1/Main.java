package task6.part1;

import task3.Substitution;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class Main
{

	public static final String ALPHABET = "abcd";

	private static final ArrayList<Substitution> substitutions = getInput();

	public static void main(String[] args)
	{

		Scanner scanner = null;
		Set<String> from = new HashSet<>(  );
		Set<String> to = new HashSet<>(  );
		try
		{
			scanner = new Scanner(new File( "/Users/estrepetov/University/course_5/university/Mironov/src/task6/part1/part1_input.txt" ) );
			while (scanner.hasNext()) {
				from.add( new StringBuilder( scanner.nextLine().replace( " ", "" ) ).reverse()
								.toString() );
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		try
		{
			scanner = new Scanner(new File( "/Users/estrepetov/University/course_5/university/Mironov/src/task6/part1/part2_input.txt" ) );
			while (scanner.hasNext()) {
				to.add( scanner.nextLine().replace( " ", "" ) );
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		for (String s : from)
		{
			if(! to.contains( s )) {
				System.out.println(s);
			}
		}

		System.out.println();

		for (String s : to)
		{
			if (! from.contains( s )) {
				System.out.println(s);
			}
		}
	}

	private static void substitute(String axiom, String history)
	{
		for (Substitution substitution : substitutions)
		{
			if (axiom.contains( substitution.getFrom() ))
			{
				String newAxiom = substitution.makeSubs( axiom );
				String newHistory = history + "<-" + newAxiom;
				System.out.println( newHistory );
				if (newAxiom.length() < 6)
				{
					substitute( newAxiom, newHistory );
				}
			}
		}
	}

	private static ArrayList<Substitution> getInput()
	{
		Scanner scanner = null;
		try
		{
			scanner = new Scanner( new FileInputStream(
							new File( "/Users/calmarj/Университет/university/Mironov/src/task6/part1/part1_input.txt" ) ) );
			ArrayList<Substitution> subs = new ArrayList<>();

			while (scanner.hasNextLine())
			{
				subs.add( new Substitution( scanner.nextLine() ) );
			}
			return subs;
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public static void iterate(char[] chars, int len, char[] build, int pos)
	{
		if (pos == len)
		{
			String word = new String( build );
			// do what you need with each word here
			return;
		}

		for (char letter : chars)
		{
			build[pos] = letter;
			iterate( chars, len, build, pos + 1 );
		}
	}

}
