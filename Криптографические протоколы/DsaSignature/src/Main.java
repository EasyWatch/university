import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;


public class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(new File("input"))));
            String line;

            String[] domainParameters = new String[3];
            domainParameters[0] = (in.readLine().split("^p="))[1];
            domainParameters[1] = (in.readLine().split("^q="))[1];
            domainParameters[2] = (in.readLine().split("^g="))[1];

          /*  System.out.println("p=" + domainParameters[0]);
            System.out.println("q=" + domainParameters[1]);
            System.out.println("g=" + domainParameters[2]);*/

            for (String domainParameter : domainParameters) {
                if (!domainParameter.matches("^[0-9]+$")) throw new Exception();
            }
            BigInteger p = new BigInteger(domainParameters[0]);
            BigInteger q = new BigInteger(domainParameters[1]);
            BigInteger g = new BigInteger(domainParameters[2]);
            if (!User.Companion.check(p, q, g)) throw new Exception();

            DSA dsa = new DSA(p, q, g);
            String action = in.readLine();
            if (action.startsWith("генерация")) {
                BigInteger[] pair = dsa.generate();
                System.out.println("x=" + pair[0]);
                System.out.println("y=" + pair[1]);

            } else if (action.startsWith("подпись")) {
                String x = (in.readLine().split("^x="))[1];
                String y = (in.readLine().split("^y="))[1];
                if (!x.matches("^[0-9]+$") || !y.matches("^[0-9]+$")) throw new Exception();
                dsa.setX(new BigInteger(x));
                dsa.setY(new BigInteger(y));
                String filePath = in.readLine();
                BigInteger[] signature = dsa.sign(filePath);
                System.out.println("r=" + signature[0]);
                System.out.println("s=" + signature[1]);

            } else if (action.equals("проверка")) {
                String y = (in.readLine().split("^y="))[1];
                if (!y.matches("^[0-9]+$")) throw new Exception();
                String d = in.readLine();
                String r = (in.readLine().split("^r="))[1];
                String s = (in.readLine().split("^s="))[1];

                if (dsa.verify(new BigInteger(y), d, new BigInteger(r), new BigInteger(s))) {
                    System.out.println("Подпись прошла проверку");
                } else {
                    System.out.println("Подпись не прошла проверку");
                }

            }

        } catch (Exception e) {
            System.out.println("Неправильные входные параметры");
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }
}
