import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;

public class ParamGenerator {

    public static void main(String[] args) throws FileNotFoundException {
        BigInteger q, p, g;
        do {
            q = generateQ();
            p = generateP(q);
            g = generateG(p, q);
        } while (!User.Companion.check(p, q, g));

        PrintWriter printWriter = new PrintWriter(new File("keys"));
        printWriter.write("p=");
        printWriter.write(p.toString() + "\n");
        printWriter.write("q=");
        printWriter.write(q.toString() + "\n");
        printWriter.write("g=");
        printWriter.write(g.toString() + "\n");
        printWriter.flush();
        System.out.println("Генерация успешно завершена");
    }

    private static BigInteger generateG(BigInteger p, BigInteger q) {
        BigInteger h = new BigInteger("2");

        BigInteger pMinusOne = p.subtract(BigInteger.ONE);

        return h.modPow(pMinusOne.divide(q), p);
    }

    private static BigInteger generateQ() {
        SecureRandom random = new SecureRandom();
        BigInteger q;
        do {
            q = new BigInteger(User.Q_BIT_LENGTH, random);

        } while (!q.isProbablePrime(200) || q.bitLength() != User.Q_BIT_LENGTH);
        return q;
    }

    private static BigInteger generateP(BigInteger q) {
        SecureRandom random = new SecureRandom();
        BigInteger p;
        BigInteger k = new BigInteger(User.P_BIT_MIN - User.Q_BIT_LENGTH + 1, random);
        do {
            p = q.multiply(k).add(BigInteger.ONE);
            k = k.add(BigInteger.ONE);
        } while (!p.isProbablePrime(200) || p.bitLength() < User.P_BIT_MIN);

        return p;
    }
}
