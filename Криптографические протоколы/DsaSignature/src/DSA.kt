import java.io.IOException
import java.math.BigInteger
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.security.SecureRandom


class DSA {
    private var user: User? = null

    constructor(p: BigInteger, q: BigInteger, g: BigInteger) {
        this.user = User(p, q, g)
    }

    constructor(p: BigInteger, q: BigInteger, g: BigInteger, x: BigInteger, y: BigInteger) {
        this.user = User(p, q, g, x, y)
    }

    fun generate(): Array<BigInteger> {
        val keys = arrayOfNulls<BigInteger>(2)
        keys[0] = this.generateX()
        keys[1] = this.g.modPow(keys[0], this.p)
        return keys
    }

    fun sign(filePath: String): Array<BigInteger> {
        val digestMessage = getHash(filePath)
        val z = BigInteger(digestMessage)
        val k = this.generateK()
        val r = this.g.modPow(k, this.p).mod(this.q)
        val s = (k.modInverse(this.q).multiply(z.add(this.x.multiply(r)))).mod(this.q)

        val signature = arrayOfNulls<BigInteger>(2)
        signature[1] = s
        signature[0] = r

        return signature
    }


    fun verify(publicKey: BigInteger, filePath: String, r: BigInteger, s: BigInteger): Boolean {
        if (r.compareTo(this.q) >= 0 || r.compareTo(BigInteger.ZERO) <= 0) return false
        if (s.compareTo(this.q) >= 0 || s.compareTo(BigInteger.ZERO) <= 0) return false

        val z = BigInteger(getHash(filePath))
        val w = s.modInverse(q)
        val u1 = z.multiply(w).mod(q)
        val u2 = r.multiply(w).mod(q)
        val v = ((this.g.modPow(u1, this.p).multiply(publicKey.modPow(u2, this.p))).mod(this.p)).mod(this.q)

        return v.compareTo(r) == 0
    }

    fun generateX(): BigInteger {
        val r = SecureRandom()
        val x: BigInteger
        do {
            x = BigInteger(q.bitLength(), r)
        } while ((this.q.compareTo(x) < 1) || (x.compareTo(BigInteger.ZERO) < 1))
        return x
    }

    fun generateK(): BigInteger {
        val r = SecureRandom()
        val k: BigInteger
        do {
            k = BigInteger(q.bitLength(), r)
        } while ((this.q.compareTo(k) < 1) || (k.compareTo(BigInteger.ZERO) < 1))

        return k
    }

    private fun getHash(filePath: String): ByteArray {
        val path = Paths.get(filePath)
        var digestMessage: ByteArray? = null
        try {
            val data = Files.readAllBytes(path)
            digestMessage = (MD5.calculateMD5(data))
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return digestMessage
    }


    val p: BigInteger
        get() {
            return this.user!!.p
        }

    val q: BigInteger
        get() {
            return this.user!!.q
        }

    val g: BigInteger
        get() {
            return this.user!!.g
        }

    var x: BigInteger
        get() {
            return this.user!!.x
        }
        set(x) {
            this.user!!.x = x
        }

    var y: BigInteger
        get() {
            return this.user!!.y
        }
        set(y) {
            this.user!!.y = y
        }
}