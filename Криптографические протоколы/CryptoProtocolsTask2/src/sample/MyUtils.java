package sample;

import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class MyUtils {

    public final static List<Pair<Integer,Float>> aData = new ArrayList<>();
    public final static List<Integer> bData = new ArrayList<>();

    public static void getData() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("/Users/calmarj/Университет/Криптографические протоколы/CryptoProtocolsTask2/src/input.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert scanner != null;
        int n = scanner.nextInt();
        int prime = scanner.nextInt();
        while (prime != n) {
            bData.add(prime);
            prime = scanner.nextInt();
        }
        bData.add(prime);
        for (int m = 3; m <= n; m += 2) {
           aData.add(new Pair<>(m, a(m) ));
        }
    }

    private static float a(int m) {
        int a = 0;
        for (int i = 3; i <= m; i = i + 2) {
            if (pow(2, (i - 1) / 2,i) == 1) {
                a++;
            }
        }
        float result = 2*(float)a/(float)(m-1);
        System.out.println(result);
        return result;
    }

    private static long pow(int a, int n,int p) {
        long result = a;
        for (int i = 1; i < n; i++) {
            result *= a;
            result %=p;
        }
        result%=p;
        return result;
    }
}
