package sample;

import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.util.Pair;

import java.util.List;

public class Controller {
    public LineChart<Number,Number> lineChart;
    public NumberAxis xAxis;
    public NumberAxis yAxis;

    @FXML
    public void initialize() {
        lineChart.setTitle("Криптографические протоколы: Задание 2");
        lineChart.getStylesheets().add("lol.css");

        XYChart.Series<Number, Number> aSeries = new XYChart.Series<>();
        List<Pair<Integer,Float>> data = MyUtils.aData;
        aSeries.setName("a(m)");
        for (Pair<Integer, Float> pair : data) {
            aSeries.getData().add(new XYChart.Data<>(pair.getKey(),pair.getValue()));
        }

        XYChart.Series<Number,Number> bSeries = new XYChart.Series<>();
        bSeries.setName("b(m)");
        for (int i = 0; i < MyUtils.bData.size(); i++) {
            final Integer primeNumber = MyUtils.bData.get(i);
            bSeries.getData().add(new XYChart.Data<>(primeNumber,(float) (i/(primeNumber *1.0))));
        }

        lineChart.getData().add(aSeries);
        lineChart.getData().add(bSeries);

        aSeries.nodeProperty().get().setStyle(
                "-fx-background-color: mediumvioletred;");

    }


}
