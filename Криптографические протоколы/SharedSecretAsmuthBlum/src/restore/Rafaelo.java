package restore;

import utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Scanner;

public class Rafaelo {

    public static final String INPUT_FILE_NAME = "src/restore/restore_input";

    private static int m;
    private static BigInteger d[];
    private static BigInteger k[];
    private static BigInteger p;

    public static void main(String[] args) {
        getInput();
        BigInteger M = restoreSecret();
        String message = Utils.transformMessageFromNumber(M);
        System.out.println("Сообщение:"+message);
    }

    private static void getInput() {
        try {
            Scanner scanner = new Scanner(new File(INPUT_FILE_NAME));
            m = scanner.nextInt();
            scanner.nextLine();
            d = new BigInteger[m];
            k = new BigInteger[m];
            for (int i = 0; i < m; i++) {
                String userLine = scanner.nextLine();
                String [] params = userLine.split(",");
                p = new BigInteger(params[0].trim());
                d[i] = new BigInteger(params[1].trim());
                k[i] = new BigInteger(params[2].trim());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static BigInteger restoreSecret() {
        BigInteger M0 = d[0];
        for (int i = 1; i < m; i++) {
            M0 = M0.multiply(d[i]);
        }

        BigInteger[] Mi = new BigInteger[m];
        for (int i = 0; i < m; i++) {
            Mi[i] = M0.divide(d[i]);
        }

        BigInteger[] y = new BigInteger[m];
        for (int i = 0; i < m; i++) {
            BigInteger[] vals = Utils.extendGCD(Mi[i], d[i]);
            y[i] = vals[1];
        }

        BigInteger x = k[0].multiply(Mi[0].multiply(y[0]));
        for (int i = 1; i < m; i++) {
            x = x.add(k[i].multiply(Mi[i].multiply(y[i])));
        }
        x = x.mod(M0);

        return x.mod(p);
    }

}
