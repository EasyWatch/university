package share;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

import utils.Utils;


public class Donatello {

    private static final int MAX_RANDOM_INT = 20;
    public static final String OUTPUT_FILE = "src/share/share_output";

    private static int n;
    private static int m;
    private static BigInteger M;

    private static BigInteger p;
    private static BigInteger M_1;
    private static BigInteger[] d;
    private static BigInteger[] k;

    private static PrintWriter outWriter;

    public static void main(String[] args) {
        getInput();
        shareSecret();
    }

    private static void shareSecret() {
        p = getPrimeNumber();
        outWriter.println("p: " + p);

        d = getD();

        int r = getRandomNumber();
        outWriter.println("r: " + r);

        M_1 = M.add(p.multiply(
                new BigInteger(Integer.toString(r)))
        );
        outWriter.println("M': " + M_1);

        k = getRemainders();
        outWriter.println("\nПользователи:");
        for (int i = 0; i < n; i++) {
            outWriter.println("№"+(i+1) +": "+ p + ", " + d[i] + ", " + k[i]);
        }
        outWriter.flush();
    }

    private static BigInteger getPrimeNumber() {
        return M.nextProbablePrime();
    }

    private static BigInteger[] getD() {
        BigInteger[] divisors = new BigInteger[n];

        divisors = getCheckedDivisors(divisors, p, 0);

        return divisors;
    }



    private static BigInteger[] getCheckedDivisors(BigInteger[] divisors, BigInteger first, int index) {
        divisors[0] = first.nextProbablePrime();

        for (int i = 1; i < n; i++) {
            divisors[i] = divisors[i - 1].nextProbablePrime();
            if (!isCoPrime(divisors, i)) {
                divisors[i] = divisors[i].nextProbablePrime();
            }
        }

        BigInteger left = divisors[0];
        for (int i = 1; i < m; i++) {
            left = left.multiply(divisors[i]);
        }

        BigInteger right = divisors[n - m + 1].multiply(p);
        for (int i = n - m + 2; i < n; i++) {
            right = right.multiply(divisors[i]);
        }

        if (left.compareTo(right) != 1) {
            divisors = getCheckedDivisors(divisors, divisors[index], ++index);
        }

        return divisors;
    }

    private static boolean  isCoPrime(BigInteger[] divisors, int i) {
        BigInteger result = divisors[0];
        for (int j = 1; j <= i; j++) {
            result = result.gcd(divisors[j]);
        }

        return result.compareTo(BigInteger.ONE) == 0;
    }


    private static void getInput() {
        Scanner scanner = new Scanner(System.in);
        try {
            outWriter = new PrintWriter(new File(OUTPUT_FILE));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.print("Введите секрет:");
        String message = scanner.nextLine().toUpperCase();
        if (Utils.checkMessageForAlphabet(message)) {
            M = new BigInteger(String.valueOf(Utils.transformMessageToNumber(message)));
            System.out.println(M.toString());
        }
        else {
            System.err.println("ТАКОЙ СЕКРЕТ НАМ НЕ НУЖЕН!!!!");
            System.exit(-1);

        }
        try {
            System.out.print("Введите m:");
            m = scanner.nextInt();
            System.out.print("Введите n:");
            n = scanner.nextInt();

            if (m > n||n<=0||m<=0) {
                throw new Exception();
            }
        }
        catch (Exception ex) {
            System.err.println("m и n должны быть целым положительными числами, m < n");
            System.exit(-1);
        }

    }

    private  static int getRandomNumber() {
        return new Random().nextInt(MAX_RANDOM_INT);
    }



    private static void outputArray(BigInteger[] array) {
        for (BigInteger bigInteger : array) {
            outWriter.println(bigInteger.toString());
        }
    }

    private static BigInteger[] getRemainders() {
        BigInteger[] remainders = new BigInteger[n];

        for (int i = 0; i < n; i++) {
            remainders[i] = M_1.mod(d[i]);
        }

        return remainders;
    }


}
