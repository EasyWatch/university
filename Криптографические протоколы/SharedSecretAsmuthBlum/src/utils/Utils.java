package utils;

import java.math.BigInteger;


public class Utils {

    public static final String ALPHABET = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ1234567890";

    public static BigInteger transformMessageToNumber(String message) {
        String integerString = "";
        for (char letter : message.toCharArray()) {
            integerString += transformLetterToNumber(letter);
        }
        return  new BigInteger(integerString);
    }

    private static String transformLetterToNumber(char letter) {
        if (letter != ' ') {
            int itemLength = (int) Math.log10(ALPHABET.length())+1;
            String letterIndex = ((ALPHABET.indexOf(letter))) + "";
            int zeroesCount = itemLength - letterIndex.length();
            for (int i = 0; i < zeroesCount; i++) {
                letterIndex = "0" + letterIndex;
            }
            return letterIndex;
        } else {
            return "";
        }
    }

    public static boolean checkMessageForAlphabet(String message) {
        for (char letter : message.toCharArray()) {
            if (letter != ' '&& ALPHABET.indexOf(letter) == -1) {
                return false;
            }
        }
        return true;
    }


    public static String transformMessageFromNumber(BigInteger m) {
        String message = "";
        int itemLength = (int) Math.log10(ALPHABET.length())+1;
        String messageString = m.toString();
        if (messageString.length() % itemLength != 0) {
            int zeroesCount = messageString.length() %itemLength;
            for (int i = 0; i < zeroesCount; i++) {
                messageString = "0" + messageString;
            }
        }
        String[] letters = messageString.split("(?<=\\G.{"+itemLength+"})");
        for (String letter : letters) {
            final Character letterFromNumber = transformLetterFromNumber(letter);
            if (letterFromNumber != null) {
                message+=letterFromNumber;
            }
            else {
                return null;
            }
         }
        return message;
    }

    private static Character transformLetterFromNumber(String letter) {
        int index = (Integer.parseInt(letter));
        if (index<0||index>ALPHABET.length()-1) {
            return null;
        }
        else {
            return ALPHABET.charAt(index);
        }
    }

    public static BigInteger[] extendGCD(BigInteger p, BigInteger q) {
        if (q.compareTo(BigInteger.ZERO) == 0)
            return new BigInteger[] { p, BigInteger.ONE, BigInteger.ZERO };

        BigInteger[] vals = extendGCD(q, p.mod(q));
        BigInteger d = vals[0];
        BigInteger a = vals[2];
        BigInteger b = vals[1].subtract(p.divide(q).multiply(vals[2]));
        return new BigInteger[] { d, a, b };
    }


}
