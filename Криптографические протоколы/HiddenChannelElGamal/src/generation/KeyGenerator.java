package generation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Scanner;

public class KeyGenerator {

    private static final SecureRandom random = new SecureRandom();

    private static final String OUPUT_FILE_PATH = "src/generation/keys";

    public static void main(String[] args) {
        int bitCount = getBitCount();

        BigInteger p = generateP(bitCount);

        BigInteger g = generateG(p);

        BigInteger r = generateR(p);

        BigInteger k = getK(g,r,p);

        printOutput(p,g,r,k);

    }

    private static BigInteger generateP(int bitCount) {

        return BigInteger.probablePrime(bitCount,random);
    }

    private static BigInteger generateG(BigInteger p) {
        BigInteger g;
        do {
            g = new BigInteger(p.bitCount(), random);
        }while (g.compareTo(p)>0);
        return g;
    }


    private static BigInteger generateR(BigInteger p) {
        BigInteger r;
        do {
            r = new BigInteger(p.bitCount(), random);
        }while (r.compareTo(p)>0);
        return r;
    }

    private static BigInteger getK(BigInteger g, BigInteger r, BigInteger p) {
        return g.modPow(r,p);
    }

    private static void printOutput(BigInteger p, BigInteger g, BigInteger r, BigInteger k) {
        try {
            PrintWriter printWriter = new PrintWriter(new File(OUPUT_FILE_PATH));
            printWriter.write("Открытый ключ:\n");
            printWriter.write("k = ");
            printWriter.write(k+"\n");
            printWriter.write("g = ");
            printWriter.write(g+"\n");
            printWriter.write("p = ");
            printWriter.write(p+"\n\n");
            printWriter.write("Закрытый ключ:\n");
            printWriter.write("r = "+r+"\n");
            printWriter.flush();

            System.out.println("Генерация прошла успешно");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static  int getBitCount() {
        int bitCount = 0;
        System.out.print("Введите количество бит числа p:");
        Scanner scanner = new Scanner(System.in);

        try {
            bitCount = scanner.nextInt();
            if (bitCount<0) {
                throw new Exception();
            }
        }
        catch (Exception ex) {
            System.err.println("Ошибка! Количество бит должен быть целым положительным числом");
            System.exit(-1);
        }
        return bitCount;
    }
}
