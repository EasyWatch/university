package encryption;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.List;
import java.util.Scanner;
import utils.Utils;

public class Encryptor {

    public static final String INPUT_FILE_NAME = "src/encryption/encryption_input";
    public static final String OUTPUT_FILE_NAME = "src/encryption/encryption_output";

    private static BigInteger p;
    private static BigInteger g;
    private static BigInteger k;
    private static BigInteger r;


    public static void main(String[] args) {
        if (getEncryptionInput()) {
            String message = getSecretMessage();
            if (isValidMessage(message)) {
              encrypt(message);
            }
            else {
                System.out.println("Данное сообщение не подходит. Шифрование провалилось");
            }
        }
    }

    private static void encrypt(String message) {
        BigInteger pMinusOne = p.subtract(BigInteger.ONE);
        BigInteger M = Utils.transformMessageToNumber(message);
        BigInteger x = g.modPow(M,p);
        BigInteger inverseM = M.modInverse(pMinusOne);
        BigInteger M_1 = generateM_1();
        BigInteger y = M_1.subtract(r.multiply(x).mod(pMinusOne)).multiply(inverseM).mod(pMinusOne);

        printOutput(x,y,M_1);
    }

    private static void printOutput(BigInteger x, BigInteger y, BigInteger m_1) {
        try {
            PrintWriter printWriter = new PrintWriter(new File(OUTPUT_FILE_NAME));
            printWriter.write("Сообщение = "+m_1+"\n");
            printWriter.write("Подпись: \n");
            printWriter.write("x = "+x+"\n");
            printWriter.write("y = "+y+"\n");
            printWriter.flush();
            System.out.println("Шифрование прошло успешно");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }



    private static BigInteger generateM_1() {
        BigInteger M_1;
        do {
            M_1 = new BigInteger(p.bitCount(), new SecureRandom());
        }while (M_1.compareTo(p)>0);
        return M_1;
    }

    private static boolean isValidMessage(String message) {
        if (!Utils.checkMessageForAlphabet(message)) return false;
        BigInteger M = Utils.transformMessageToNumber(message);
        return !((M.gcd(p).compareTo(BigInteger.ONE) != 0) || (M.gcd(p.subtract(BigInteger.ONE)).compareTo(BigInteger.ONE) != 0));
    }




    private static String getSecretMessage() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите секретное сообщение:");
        return scanner.nextLine().toUpperCase();
    }

    private static boolean getEncryptionInput() {
        try {
            List<String> encryptionInput = Files.readAllLines(Paths.get(INPUT_FILE_NAME));
            for (String line : encryptionInput) {
                final String[] split = line.split("=");
                try {
                    final String param = split[0].trim();
                    final String value = split[1].trim();
                    switch (param) {
                        case "p": {
                            p = new BigInteger(value);
                            break;
                        }
                        case "g": {
                            g = new BigInteger(value);
                            break;
                        }
                        case "k": {
                            k = new BigInteger(value);
                            break;
                        }
                        case "r": {
                            r = new BigInteger(value);
                            break;
                        }

                        default: {
                            System.err.println("Ошибка! Неизвестный параметр " + split[0]);
                            return false;
                        }
                    }
                }
                catch (Exception ex) {
                    System.err.println("Ошибка! Неправильные входные параметры");
                    return false;
                }
            }
        } catch (IOException e) {
            System.err.println("Ошибка! Не существует файл со входным параметрами для шифрования");
            return false;
        }
        return true;
    }


}
