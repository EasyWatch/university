package decryption;

import utils.Utils;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Decryptor {

    public static final String INPUT_FILE_NAME = "src/decryption/decryption_input";
    public static final String OUTPUT_FILE_NAME = "src/decryption/decryption_output";

    private static BigInteger p;
    private static BigInteger g;
    private static BigInteger k;
    private static BigInteger r;
    private static BigInteger x;
    private static BigInteger y;
    private static BigInteger M_1;

    public static void main(String[] args) {
        if (getDecryptionInput()) {
            if (checkSign()) {
                System.out.println("Подпись прошла проверку");
                String message = decrypt();
                System.out.println("Секретное сообщение:"+message);
            }
            else {
                System.out.println("Подпись не прошла проверку");
            }
        }
    }

    private static String decrypt() {
        BigInteger pMinusOne = p.subtract(BigInteger.ONE);
        BigInteger M = M_1.subtract(r.multiply(x).mod(pMinusOne)).multiply(y.modInverse(pMinusOne)).mod(pMinusOne);
        return Utils.getMessageFromNumber(M);
    }

    private static boolean checkSign() {
        final BigInteger leftSide = k.modPow(x, p).multiply(x.modPow(y, p)).mod(p);
        final BigInteger rightSide = g.modPow(M_1, p);
        return leftSide.compareTo(rightSide) == 0;
    }


    private static boolean getDecryptionInput() {
        try {
            List<String> decryptionInput = Files.readAllLines(Paths.get(INPUT_FILE_NAME));
            for (String line : decryptionInput) {
                if (line.length() > 0) {
                    final String[] split = line.split("=");
                    try {
                        final String param = split[0].trim();
                        final String value = split[1].trim();
                        switch (param) {
                            case "p": {
                                p = new BigInteger(value);
                                break;
                            }
                            case "g": {
                                g = new BigInteger(value);
                                break;
                            }
                            case "k": {
                                k = new BigInteger(value);
                                break;
                            }
                            case "r": {
                                r = new BigInteger(value);
                                break;
                            }
                            case "x": {
                                x = new BigInteger(value);
                                break;
                            }
                            case "y": {
                                y = new BigInteger(value);
                                break;
                            }
                            case "Сообщение": {
                                M_1 = new BigInteger(value);
                                break;
                            }
                            default: {
                                System.err.println("Ошибка! Неизвестный параметр " + split[0]);
                                return false;
                            }
                        }
                    } catch (Exception ex) {
                        System.err.println("Ошибка! Неправильные входные параметры для дешифрования");
                        return false;
                    }
                }
            }
        } catch (IOException e) {
            System.err.println("Ошибка! Не существует файл со входным параметрами для дешифрования");
            return false;
        }
        return true;
    }
}
