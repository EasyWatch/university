package utils;

import java.math.BigInteger;


public class Utils {

    public static final String ALPHABET = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ1234567890";

    public static BigInteger transformMessageToNumber(String message) {
        String integerString = "";
        for (char letter : message.toCharArray()) {
            integerString += transformLetterToNumber(letter);
        }
        return  new BigInteger(integerString);
    }

    public static String transformLetterToNumber(char letter) {
        if (letter != ' ') {
            int itemLength = (int) Math.log10(ALPHABET.length())+1;
            String letterIndex = ((ALPHABET.indexOf(letter) + 1) * 2 + 1) + "";
            int zeroesCount = itemLength - letterIndex.length();
            for (int i = 0; i < zeroesCount; i++) {
                letterIndex = "0" + letterIndex;
            }
            return letterIndex;
        } else {
            return "";
        }
    }

    public static boolean checkMessageForAlphabet(String message) {
        for (char letter : message.toCharArray()) {
            if (letter != ' '&& ALPHABET.indexOf(letter) == -1) {
                return false;
            }
        }
        return true;
    }


    public static String getMessageFromNumber(BigInteger m) {
        String message = "";
        int itemLength = (int) Math.log10(ALPHABET.length())+1;
        String[] letters = m.toString().split("(?<=\\G.{"+itemLength+"})");
        for (String letter : letters) {
            final Character letterFromNumber = getLetterFromNumber(letter);
            if (letterFromNumber != null) {
                message+=letterFromNumber;
            }
            else {
                return null;
            }
         }
        return message;
    }

    private static Character getLetterFromNumber(String letter) {
        int index = ((Integer.parseInt(letter)-1)/2)-1;
        if (index<0||index>ALPHABET.length()-1) {
            return null;
        }
        else {
            return ALPHABET.charAt(index);
        }
    }
}
