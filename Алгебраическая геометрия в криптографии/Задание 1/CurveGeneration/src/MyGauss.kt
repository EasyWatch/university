import java.math.BigInteger

class MyGauss {
    var realPart: BigInteger? = null
        private set
    var imaginaryPart: BigInteger? = null
        private set

    constructor(realPart: BigInteger, imaginaryPart: BigInteger) {
        this.realPart = realPart
        this.imaginaryPart = imaginaryPart
    }

    constructor(integer: BigInteger) {
        this.realPart = integer
        this.imaginaryPart = BigInteger.ZERO
    }

    val norm: BigInteger
        get() = realPart!!.multiply(realPart).add(imaginaryPart!!.multiply(imaginaryPart))

    fun conjugate(): MyGauss {
        return MyGauss(realPart!!, imaginaryPart!!.multiply(BigInteger("-1")))
    }

    fun add(`val`: MyGauss): MyGauss {
        return MyGauss(realPart!!.add(`val`.realPart), imaginaryPart!!.add(`val`.imaginaryPart))
    }

    fun subtract(`val`: MyGauss): MyGauss {
        return MyGauss(realPart!!.subtract(`val`.realPart), imaginaryPart!!.subtract(`val`.imaginaryPart))
    }

    fun multiply(`val`: MyGauss): MyGauss {
        return MyGauss(realPart!!.multiply(`val`.realPart).subtract(imaginaryPart!!.multiply(`val`.imaginaryPart)), realPart!!.multiply(`val`.imaginaryPart).add(imaginaryPart!!.multiply(`val`.realPart)))
    }

    private fun getClosest(a: BigInteger, d: BigInteger): BigInteger {
        val dr = a.abs().divideAndRemainder(d.abs())
        if (a.signum() < 0) {
            if (dr[1].compareTo(d.subtract(dr[1])) < 0) {
                return dr[0].multiply(BigInteger("-1"))
            } else {
                return dr[0].add(BigInteger.ONE).multiply(BigInteger("-1"))
            }
        } else {
            if (dr[1].compareTo(d.subtract(dr[1])) < 0) {
                return dr[0]
            } else {
                return dr[0].add(BigInteger.ONE)
            }
        }
    }

    fun divide(`val`: MyGauss): MyGauss {
        val numerator = this.multiply(`val`.conjugate())
        val denominator = `val`.multiply(`val`.conjugate()).realPart
        return MyGauss(getClosest(numerator.realPart!!, denominator!!), getClosest(numerator.imaginaryPart!!, denominator))
    }

    fun remainder(`val`: MyGauss): MyGauss {
        return this.subtract(this.divide(`val`).multiply(`val`))
    }

    fun divideAndRemainder(`val`: MyGauss): Array<MyGauss> {
        val quotient = this.divide(`val`)
        val remainder = this.subtract(quotient.multiply(`val`))
        return arrayOf(quotient, remainder)
    }

    fun gcd(`val`: MyGauss): MyGauss {
        var a = this
        var b = `val`
        while (b.norm != BigInteger.ZERO) {
            val remainder = a.remainder(b)
            a = b
            b = remainder
        }
        return a
    }

    fun pow(n: Int): MyGauss {
        var n = n
        var current = this
        var result = MyGauss(BigInteger.ONE, BigInteger.ZERO)
        while (n > 0) {
            if ((n and 1) == 0) {
                current = current.multiply(current)
                n = n shr 1
            } else {
                result = result.multiply(current)
                n--
            }
        }
        return result
    }

    override fun toString(): String {
        if (realPart!!.signum() == 0) {
            if (imaginaryPart!!.signum() == 0) {
                return "0"
            } else {
                return imaginaryPart!!.toString() + "i"
            }
        } else {
            if (imaginaryPart!!.signum() == 0) {
                return realPart!!.toString()
            } else {
                if (imaginaryPart!!.signum() > 0) {
                    return realPart!!.toString() + " + " + imaginaryPart!!.toString() + "i"
                } else {
                    return realPart!!.toString() + " - " + imaginaryPart!!.abs().toString() + "i"
                }
            }
        }
    }
}
