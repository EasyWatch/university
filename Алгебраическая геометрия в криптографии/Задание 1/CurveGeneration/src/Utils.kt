import java.math.BigInteger
import kotlin.math.div
import kotlin.math.minus
import kotlin.math.plus
import kotlin.math.times

object Utils {
    private val ONE = BigInteger.ONE
    private val TWO = BigInteger("2")
    private val EIGHT = BigInteger("8")

    fun legendreSymbol(a: BigInteger, p: BigInteger): BigInteger {
        return a.modPow((p-ONE).divide(TWO), p)
    }

    fun inverse(a: BigInteger, p: BigInteger): BigInteger {
        return a.modPow(p.subtract(TWO), p)
    }

    private fun solveSquareComparsionWithPEqualsTo1Mod8(a: BigInteger, p: BigInteger): Array<BigInteger> {
        var k = 0
        var h = p.subtract(ONE)
        while (h.mod(TWO) == BigInteger.ZERO) {
            k++
            h = h.divide(TWO)
        }
        var N = ONE
        while (true) {
            if (legendreSymbol(N, p) != ONE) {
                break
            }
            N = N.add(ONE)
        }

        val a1 = a.modPow(h.add(ONE).divide(TWO), p)
        val a2 = inverse(a, p)
        val N1 = N.modPow(h, p)
        var N2 = ONE
        var j: BigInteger
        for (i in 0..k - 2) {
            val b = a1.multiply(N2).mod(p)
            val c = a2.multiply(b.pow(2)).mod(p)
            val d = c.modPow(TWO.pow(k - 2 - i), p)
            if (d == ONE) {
                j = BigInteger.ZERO
            } else {
                j = ONE
            }
            N2 = N2.multiply(N1.modPow(j.multiply(TWO.pow(i)), p)).mod(p)
        }
        val x1 = a1.multiply(N2).mod(p)
        val x2 = p.subtract(x1)
        return arrayOf(x1, x2)
    }

    private fun solveSquareComparsionWithPEqualsTo5Mod8(a: BigInteger, p: BigInteger): Array<BigInteger> {
        val m = p/EIGHT
        val x1: BigInteger
        val x2: BigInteger
        if (a.modPow(m*TWO+ONE, p) == ONE) {
            x1 = a.modPow(m+ONE, p)
        } else {
            x1 = a.modPow(m+ONE, p)*(TWO.modPow(m*TWO+ONE, p)).mod(p)
        }
        x2 = p.subtract(x1)
        return arrayOf(x1, x2)
    }

    fun solveSquareComparsion(a: BigInteger, p: BigInteger): Array<BigInteger> {
        if (!p.isProbablePrime(100)) {
            throw IllegalArgumentException("P не является простым")
        }
        if (legendreSymbol(a, p) != ONE) {
            throw IllegalArgumentException("Сравнение не имеет корней, т.к. a - квадратический невычет")
        }
        if (p.mod(EIGHT) == ONE) {
            return solveSquareComparsionWithPEqualsTo1Mod8(a, p)
        }
        if (p.mod(EIGHT) == BigInteger("5")) {
            return solveSquareComparsionWithPEqualsTo5Mod8(a, p)
        }
        throw IllegalArgumentException("")
    }
}
