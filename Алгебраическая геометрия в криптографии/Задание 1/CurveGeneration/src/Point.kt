import java.math.BigInteger

class Point {
    var x: BigInteger? = null
        private set
    var y: BigInteger? = null
        private set

    private constructor() {
        x = null
        y = null
    }

    constructor(x: BigInteger, y: BigInteger) {
        this.x = x
        this.y = y
    }

    override fun toString(): String {
        if (x != null && y != null) {
            return "(" + x!!.toString() + ", " + y!!.toString() + ")"
        } else {
            return "O"
        }
    }

    companion object {
        val infinityPoint = Point()
    }
}
