import java.math.BigInteger
import java.security.SecureRandom
import java.util.*
import kotlin.math.div
import kotlin.math.minus
import kotlin.math.plus

object CurveUtils {
    private val ONE = BigInteger.ONE
    private val MINUS_ONE = BigInteger("-1")
    private val TWO = BigInteger("2")
    private val FOUR = BigInteger("4")
    private val K = 30
    private val random = Random()

    fun generateCurve(n: Int): EllipticCurve {
        var p: BigInteger
        var order: BigInteger
        do {
            //шаг 1
            p = generateP(n)
            //шаг 2,3
            order = getOrder(p)
        } while (!checkOrder(order, p)) // шаг 3,4
        var ellipticCurve: EllipticCurve
        do {
            ellipticCurve = generateCurve(p, order)
        } while (!checkCurve(ellipticCurve))//шаг 7
        return ellipticCurve
    }

    fun generateP(n: Int): BigInteger {
        var p = BigInteger(n, random)
        p = p - p % FOUR + ONE
        while (!p.isProbablePrime(200)) {
            p += FOUR
        }
        return p
    }

    fun decompose(p: BigInteger): MyGauss {
        val z = findZ(p)
        val a = MyGauss(p, BigInteger.ZERO)
        val b = MyGauss(z, ONE)
        return a.gcd(b)
    }

    private fun findZ(p: BigInteger): BigInteger {
        val random = SecureRandom()
        var a = BigInteger(p.bitLength()-1,random)
        val exp = (p-ONE)/TWO
        while (a.modPow(exp,p).compareTo(p+MINUS_ONE)!=0) {
            a = BigInteger(p.bitLength()-1,random)
        }
       // println(a)
        return  a.modPow(exp/TWO,p)
    }


    fun getOrder(p: BigInteger): BigInteger {
        val pi = decompose(p)
        //println("pi = $pi")
        return p.add(ONE).subtract(pi.realPart?.multiply(TWO))
    }

    fun getOrder(p: BigInteger, n: Int): BigInteger {
        val q = p.pow(n)
        val pi = decompose(p)
        val alpha = MyGauss(pi.realPart!!.multiply(MINUS_ONE), pi.imaginaryPart!!)
        val beta = alpha.conjugate()
        val sum = alpha.pow(n).add(beta.pow(n))
        return q.add(ONE).add(sum.realPart)
    }

    fun checkOrder(order: BigInteger, p: BigInteger): Boolean {
        val dr = order.divideAndRemainder(FOUR)
        if (dr[1] != BigInteger.ZERO || !dr[0].isProbablePrime(200)) {
            return false
        }
        for (i in 1..K) {
            if (p.pow(i).subtract(ONE).mod(getOrder(p, i)) == BigInteger.ZERO) {
                return false
            }
        }
        return true
    }

    private fun generateRandomInteger(from: BigInteger, to: BigInteger): BigInteger {
        return BigInteger(to.bitCount(), random).mod(to.subtract(from)).add(from)
    }

    fun generateRandomPoint(p: BigInteger): Point {
        return Point(generateRandomInteger(ONE, p), generateRandomInteger(ONE, p))
    }

    fun calcA(point: Point, p: BigInteger): BigInteger {
        return point.y!!.pow(2).subtract(point.x!!.pow(3)).multiply(Utils.inverse(point.x!!, p))
    }

    fun checkPoint(a: BigInteger, p: BigInteger): Boolean {
        return Utils.legendreSymbol(a, p) == ONE
    }

    private fun generateCurve(p: BigInteger, order: BigInteger): EllipticCurve {
        var point: Point
        var a: BigInteger
        do {
            point = generateRandomPoint(p) //шаг 5 (часть 1)
            a = calcA(point, p) // шаг 5 (часть 2)
        } while (!checkPoint(a, p)) //шаг 6,7
        return EllipticCurve(p, order, point, a)
    }

    fun checkCurve(ellipticCurve: EllipticCurve): Boolean {
        return ellipticCurve.multiply(ellipticCurve.generator, ellipticCurve.order) == ellipticCurve.o
    }
}
