import java.math.BigInteger

class EllipticCurve(val p: BigInteger, val order: BigInteger, val generator: Point, private val a: BigInteger) {
    val o: Point

    init {
        o = Point.infinityPoint
    }

    fun add(p1: Point, p2: Point): Point {
        if (p1 == o) {
            return p2
        }
        if (p2 == o) {
            return p1
        }
        val lambda: BigInteger
        if (p1 == p2) {
            if (p1.y == BigInteger.ZERO) {
                return o
            } else {
                lambda = p1.x!!.pow(2).multiply(THREE).add(a).multiply(Utils.inverse(p1.y!!.multiply(TWO), p)).mod(p)
            }
        } else {
            if (p1.x == p2.x) {
                return o
            } else {
                lambda = p2.y!!.subtract(p1.y).multiply(Utils.inverse(p2.x!!.subtract(p1.x), p)).mod(p)
            }
        }
        val x = lambda.pow(2).subtract(p1.x).subtract(p2.x).mod(p)
        val y = lambda.multiply(p1.x!!.subtract(x)).subtract(p1.y).mod(p)
        return Point(x, y)
    }

    fun multiply(point: Point, k: BigInteger): Point {
        var point = point
        var k = k
        var result = o
        while (k != BigInteger.ZERO) {
            if (k.mod(TWO) == BigInteger.ZERO) {
                point = add(point, point)
                k = k.divide(TWO)
            } else {
                result = add(result, point)
                k = k.subtract(BigInteger.ONE)
            }
        }
        return result
    }

    companion object {
        private val TWO = BigInteger("2")
        private val THREE = BigInteger("3")
    }
}
