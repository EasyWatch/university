import java.io.File
import java.io.FileNotFoundException
import java.io.PrintWriter
import java.util.Scanner


object Main {
    @Throws(FileNotFoundException::class)
    @JvmStatic fun main(args: Array<String>) {
        println("Введите длину характеристики поля p:")
        val scanner = Scanner(System.`in`)
        val n = scanner.nextInt()
        val curve = CurveUtils.generateCurve(n)
        println("p = " + curve.p)
        println("порядок = " + curve.order)
        println("генератор = " + curve.generator)
        var point = curve.generator
        val xWriter = PrintWriter(File("x"))
        val yWriter = PrintWriter(File("y"))
        for (i in 0..curve.order.toLong() * 2 + 1 - 1) {
            println(point)
            xWriter.write("${point.x!!} \n")
            yWriter.write("${point.y!!} \n")
            point = curve.add(point, curve.generator)
        }
        xWriter.flush()
        yWriter.flush()
    }
}
