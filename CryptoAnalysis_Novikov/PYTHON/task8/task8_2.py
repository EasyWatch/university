#http://ikit.edu.sfu-kras.ru/files/15/l15.pdf

from util import *


def generate_db(t, output_filename):
    output = open(output_filename, "w")

    # primes = historic(t * t)
    primes = generate_primes(t * t + 1)
    primes.remove(2)

    db = []
    count = 0
    element = 1
    for prime in primes:
        element = prime * element
        count += 1
        if count == t:
            count = 0
            db.append(element)
            element = 1

    for element in db:
        output.write(str(element) + "\n")

    output.flush()
    output.close()


try:
    t = int(input("Введите t(число простых сомножителей):"))
    output_filename = input("Введите путь до файла, куда сохранить базу данных: ")

    generate_db(t, output_filename)

    print("Генерация базы данных успешно завершена")
except ValueError:
    print("Ошибка! Неправильно введены входные данные")
except:
    print("Ошибка! Произошла ошибка во время работы программы")