# coding=utf-8
from math import *


def pollard_rho(n, c, f):
    a = c
    b = c
    g = 1
    safe_dict = {}
    while g == 1:
        safe_dict["x"] = a
        a = eval(f, None, safe_dict) % n
        safe_dict["x"] = b
        b = eval(f, None, safe_dict) % n
        safe_dict["x"] = b
        b = eval(f, None, safe_dict) % n
        g = gcd(abs(a - b), n)
    return g

try:
    n = int(input("Введите нечетное число n:"))
    c = int(input("Введите начальное значение c:"))
    if n <= c or c < 1:
        print("Начальное число с должно удовлетворять неравенству 1<c<n")
        exit(-1)
    f = input("Введите функцию f(x) = ")

    d = pollard_rho(n, c, f)

    if d == n:
        print("Делитель не найден")
    else:
        print(d)
except ValueError:
    print("Ошибка! Неправильно введены входные данные")
except:
    print("Ошибка! Произошла ошибка во время работы программы")
