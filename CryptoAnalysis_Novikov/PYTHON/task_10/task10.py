# coding=utf-8
import random
from math import floor
from math import gcd
from math import log

ATTEMPT_COUNT = 50


def pollard(n, B):
    a = random.randint(2, n - 2)
    d = gcd(a, n)
    if d >= 2:
        return d
    for p in B:
        l = floor(log(n) / log(p))
        a = pow(a, pow(p, l), n)
    d = gcd(a - 1, n)
    return d

try:
    n = int(input("Введите нечетное число n:"))
    primes_file_name = input("Введите путь до файла с простыми числами:")
    primes_file = open("%s" % primes_file_name, 'r')

    primes = []

    for prime_line in primes_file:
        primes.append(int(prime_line))

    p = 1
    for i in range(1, ATTEMPT_COUNT):
        p = pollard(n, primes)
        if p != 1 and p != n:
            break

    if p == 1 or p == n:
        print("Делитель не найден")
    else:
        print(p)
except (ValueError,FileNotFoundError):
    print("Ошибка! Неправильно введены входные данные")
except:
    print("Ошибка! Произошла ошибка во время работы программы")