from util import primes

B = int(input("Введите границу B:"))

primes = primes(B)

output_file_name = input("Введите путь до файла, куда хотите сохранить простые числа:")

output = open(output_file_name,"w")

for prime in primes:
    output.write(str(prime)+"\n")

output.flush()
output.close()
