from math import floor


def prime(i, primes):
    for prime in primes:
        if not (i == prime or i % prime):
            return False
    primes.add(i)
    return i


def generate_primes(n):
    primes = {2}
    i, p = 2, 0
    while True:
        if prime(i, primes):
            p += 1
            if p == n:
                return primes
        i += 1


def primes(n):
    if n == 2:
        return [2]
    elif n < 2:
        return []
    s = list(range(3, n + 1, 2))
    mroot = n ** 0.5
    half = (n + 1) / 2 - 1
    i = 0
    m = 3
    while m <= mroot:
        if s[i]:
            j = int((m * m - 3) / 2)
            s[j] = 0
            while j < half:
                s[j] = 0
                j += m
        i += 1
        m = 2 * i + 3
    return [2] + [x for x in s if x]


def gcd(a, b):
    while b != 0:
        t = b
        b = a % b
        a = t
    return a


def xgcd(a, b):
    x = 0
    y = 1
    last_x = 1
    last_y = 0

    while b != 0:
        temp = b
        quotient = int(floor(a / b))
        b = a % b
        a = temp
        temp = x
        x = last_x - quotient * x
        last_x = temp
        temp = y
        y = last_y - quotient * y
        last_y = temp

    return [last_x, last_y]


def pow_mod(a, b, c):
    x = 1
    while b > 0:
        if b & 1 == 1: x = (x * a) % c
        a = (a * a) % c
        b >>= 1
    return x % c


def index_of_coincidence(y, z):
    if len(y) != len(z):
        print("Ошибка! Разные длины последовательностей для подсчета индекса совпадения")
        exit(-1)
    f = lambda pair: 1 if pair[0] == pair[1] else 0
    n = 1/(len(y)-1)*sum((map(f, zip(y,z))))
    return n