package task5;

import cipher.VigenereCipher;
import util.MyUtils;
import util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Task5_2 {

    public static final String OPEN_TEXT_INPUT_FILE_PATH = "resources/task5/small_open_text.txt";
    public static final String ENCRYPTED_INPUT_FILE_PATH = "resources/task5/encrypted.txt";
    public static final String H0_INPUT_FILE_PATH = "resources/task5/h0_output.txt";

    public static final String HD_OUTPUT_FILE_PATH = "resources/task5/hd_output.txt";

    public static final String ENCRYPTION_KEY = "MONKEY";
    public static final String ALPHABET = " (),-.1234?ABCDEFGHIJKLMNOPQRSTUVWXYZ—’“”";


    public static void main(String[] args) throws FileNotFoundException {
        encryptText();
        calculatePeriod();
    }

    private static void encryptText() throws FileNotFoundException {
        String openText = StringUtils.getInputFromFileWithSpaces(OPEN_TEXT_INPUT_FILE_PATH);
        String encryptedText = VigenereCipher.encrypt(openText,ENCRYPTION_KEY,ALPHABET);

        PrintWriter printWriter = new PrintWriter(ENCRYPTED_INPUT_FILE_PATH);
        printWriter.print(encryptedText);
        printWriter.flush();
        printWriter.close();
    }

    private static void calculatePeriod() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите диапазон возможных значений периода [n1,n2]:\nn1 = ");
        int n1 = Integer.parseInt(scanner.nextLine());
        System.out.print("n2 = ");
        int n2 = Integer.parseInt(scanner.nextLine());

        List<Double> h0List = MyUtils.readH0List(H0_INPUT_FILE_PATH);

        String encryptedText = StringUtils.getInputFromFileWithSpaces(ENCRYPTED_INPUT_FILE_PATH);

        double rightD = -1;
        double minDifference = Double.MAX_VALUE;
        List<Integer> rightHDList = new ArrayList<>();

        for (int d = n1; d <= n2;d ++) {
            List<Integer> Z = new ArrayList<>();
            for (int i = 0; i < encryptedText.length() - d; i++) {
                int z_i = (ALPHABET.indexOf(encryptedText.charAt(i)) - ALPHABET.indexOf(encryptedText.charAt(i+d))+ALPHABET.length()) %ALPHABET.length();
                Z.add(z_i);
            }
            List<Integer> frequencyZ = new ArrayList<>();
            for (int i = 0; i < ALPHABET.length(); i++) {
                frequencyZ.add(Collections.frequency(Z,i));
            }
            double difference = 0;
            for (int i = 0; i < ALPHABET.length(); i++) {
                difference+= (frequencyZ.get(i) - h0List.get(i))*(frequencyZ.get(i) - h0List.get(i));
            }
            if (difference<minDifference) {
                rightD = d;
                minDifference = difference;
                rightHDList = frequencyZ;
            }
        }
        try {
            PrintWriter printWriter = new PrintWriter(HD_OUTPUT_FILE_PATH);
            printWriter.println("d = " + rightD);

            for (int i = 0; i < rightHDList.size(); i++) {
                printWriter.println(String.format("P(%d)_Z = %d",i,rightHDList.get(i)));
            }
            printWriter.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
