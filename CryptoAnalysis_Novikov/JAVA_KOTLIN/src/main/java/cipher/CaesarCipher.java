package cipher;

import util.MyConstants;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class CaesarCipher {

    public static String ALPHABET = MyConstants.ENGLISH_ALPHABET;
    public static void main(String[] args) {

        String str = "The quick brown fox Jumped over the lazy Dog";

        System.out.println(CaesarCipher.encode(str, 12,ALPHABET));
        System.out.println(CaesarCipher.decode(CaesarCipher.encode(str, 12,ALPHABET), 12,ALPHABET));
    }

    public static String decode(String enc, int offset,String alphabet) {
        return encode(enc, alphabet.length()-offset,alphabet);
    }

    public static String encode(String enc, int offset,String alphabet) {
        offset = (offset + alphabet.length()) % alphabet.length();
        StringBuilder encoded = new StringBuilder();
        for (char i : enc.toCharArray()) {
            if (Character.isLetter(i)) {
                    encoded.append((char) ( + (i - alphabet.charAt(0) + offset) % alphabet.length() ));

            } else {
                encoded.append(i);
            }
        }
        return encoded.toString();
    }



}
