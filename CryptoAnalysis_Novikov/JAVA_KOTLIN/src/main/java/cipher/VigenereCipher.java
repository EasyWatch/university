package cipher;

import util.MyConstants;
import util.StringUtils;

import java.io.File;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class VigenereCipher {

    private static String ALPHABET = MyConstants.ENGLISH_ALPHABET;

    public static String encrypt(String text, final String key, File alphabetFile) {
        ALPHABET = StringUtils.getInputFromFile(alphabetFile);
        return encrypt(text,key);
    }

    public static String encrypt(String text, final String key, String alphabet) {
        ALPHABET = alphabet;
        return encrypt(text,key);
    }

    public static String decrypt(String text, final String key, String alphabet) {
        ALPHABET = alphabet;
        return decrypt(text, key);
    }

    public static String encrypt(String text, final String key) {
        String res = "";
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            final int letterIndex = ALPHABET.indexOf(c);
            if (letterIndex <0) continue;
            final int keyIndex = ALPHABET.indexOf(key.charAt(j));
            if (keyIndex<0) {
                System.err.println("WARN!!!! Символ ключа не в алфавите");
            }
            final int index = (letterIndex + keyIndex) % ALPHABET.length();
            res += ALPHABET.charAt(index);
            j = ++j % key.length();
        }
        return res;
    }

    public static String decrypt(String text, final String key, File alphabetFile) {
        ALPHABET = StringUtils.getInputFromFile(alphabetFile);
        return decrypt(text,key);
    }

    public static String decrypt(String text, final String key) {
        String res = "";
        text = text.toUpperCase();
        for (int i = 0, j = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            final int letterIndex = ALPHABET.indexOf(c);
            if (letterIndex < 0) continue;
            final int keyIndex = ALPHABET.indexOf(key.charAt(j));
            if (keyIndex<0) {
                System.err.println("WARN!!!! Символ ключа не в алфавите");
            }
            final int index = (letterIndex - keyIndex + ALPHABET.length()) % ALPHABET.length();
            res += ALPHABET.charAt(index);
            j = ++j % key.length();
        }
        return res;
    }
}
