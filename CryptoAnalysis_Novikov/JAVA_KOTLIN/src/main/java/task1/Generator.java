package task1;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
import java.util.ArrayList;

public class Generator {
    public static ArrayList<int[]> ans = new ArrayList<int[]>();

    public static void swap(int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static boolean isCorrect(int[] arr)
    {
        int first = arr[0];
        int temp = first;
        int i = 0;
        while (i < arr.length && first != arr[temp - 1]) {
            i++;
            temp = arr[temp - 1];
        }
        return i == arr.length - 1 && first == arr[temp - 1];
    }

    public static void Generate(int k, int n, int[] arr)
    {
        if (k == n) {
            if (isCorrect(arr)) {
                ans.add((int[]) arr.clone());
            }
        } else {
            for (int j = k; j < arr.length; j++) {
                swap(arr, k, j);
                Generate(k + 1, n, arr);
                swap(arr, k, j);
            }
        }

    }
}
