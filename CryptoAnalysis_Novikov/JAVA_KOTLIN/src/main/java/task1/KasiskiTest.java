package task1;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
import util.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KasiskiTest {

    private static final int MIN_BLOCK_LENGTH = 3;
    private static final int MAX_BLOCK_LENGTH = 10;

    public static int test(String cipherText) {
        for (int i = MIN_BLOCK_LENGTH; i < Math.min(MAX_BLOCK_LENGTH, cipherText.length()); i++) {
            int keyLength = test(cipherText, i);
            if (keyLength > 1) {
                return keyLength;
            }
        }
        return cipherText.length();
    }

    private static int test(String cipherText, int blockLength) {
        Map<String, List<Integer>> positions = new HashMap<>();

        for (int i = 0; i < cipherText.length() - blockLength + 1; i++) {
            String block = cipherText.substring(i, i + blockLength);
            if (!positions.containsKey(block)) {
                positions.put(block, new ArrayList<>());
            }
            positions.get(block).add(i);
        }

        int keyLength = 0;

        for (List<Integer> currentPositions : positions.values()) {
            for (int i = 1; i < currentPositions.size(); i++) {
                currentPositions.set(i, currentPositions.get(i) - currentPositions.get(0));
            }
            for (int i = 1; i < currentPositions.size(); i++) {
                keyLength = MathUtils.gcd(keyLength, currentPositions.get(i));
            }
        }

        return keyLength;
    }
}
