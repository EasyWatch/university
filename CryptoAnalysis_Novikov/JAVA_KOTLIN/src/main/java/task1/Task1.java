package task1;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
import util.MathUtils;
import util.MyUtils;
import util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Task1 {
    private static final Scanner INPUT = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println(
                "Выбери задачу:\n\t1 - сгенерировать ключ;\n" +
                        "\t2 - зашифровать текст;\n" +
                        "\t3 - расшифровать текст;\n" +
                        "\t4 - выполнить тест Казиски;\n" +
                        "\t5 - перебор ключа.");
        int mode = INPUT.nextInt();
        INPUT.nextLine();
        switch (mode) {
            case 1:
                generateKey();
                break;
            case 2:
                encryptText();
                break;
            case 3:
                decryptText();
                break;
            case 4:
                kasiskiTest();
                break;
            case 5:
                findKey();
                break;
            default:
                System.out.println("Необходимо ввести число от 1 до 5.");
        }


    }

    private static void findKey() throws FileNotFoundException {
        System.out.println("Введите предполагаемую длину ключа.");
        int size = INPUT.nextInt();
      //  Scanner scanner1 = new Scanner(new File("encrypted.txt"));
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = i+1;
        }
        Generator.Generate(0, size, arr);
        PrintWriter printWriter = new PrintWriter(new File("findKey.txt"));
        String text = "";
        for (int[]shift : Generator.ans) {
            shift = m(shift);
            Arrays.stream(shift).forEach(shiftElement -> printWriter.print(shiftElement + " "));

            while (text.length() % size != 0) {
                text += text.charAt(MathUtils.Random.nextInt(text.length() - 1));
            }
            printWriter.println(decode(text, shift));
            printWriter.flush();
        }

    }

    private static int[] m(int[] arr){
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            result[i] = arr[i]-1;
        }
        return result;
    }

    private static void kasiskiTest() throws FileNotFoundException {
        System.out.println("Предполагаемая длина ключа = " + KasiskiTest.test(StringUtils.getInputFromFile("encrypted.txt")));
    }

    private static void decryptText() throws FileNotFoundException {
        String cipherText = StringUtils.getInputFromFile("encrypted.txt");
        PrintWriter printWriter = new PrintWriter(new File("decrypted.txt"));
        Scanner scanner2 = new Scanner(new File("key.txt"));

        int size = scanner2.nextInt();
        int[] shift = new int[size];
        for (int i = 0; i < size; i++) {
            shift[i]=scanner2.nextInt();
        }

        while (cipherText.length() % size != 0) {
            cipherText += cipherText.charAt(MathUtils.Random.nextInt(cipherText.length() - 1));
        }
        printWriter.print(decode(cipherText, shift));
        printWriter.flush();

    }

    private static void encryptText() throws FileNotFoundException {
        String openText = StringUtils.getInputFromFile("input.txt");
        PrintWriter cipherTextOuputWriter = new PrintWriter(new File("encrypted.txt"));
        Scanner keyScanner = new Scanner(new File("key.txt"));

        int size = keyScanner.nextInt();
        int[] shift = new int[size];
        for (int i = 0; i < size; i++) {
            shift[i]=keyScanner.nextInt();
        }

        while (openText.length() % size != 0) {
            openText += openText.charAt(MathUtils.Random.nextInt(openText.length() - 1));
        }
        cipherTextOuputWriter.print(code(openText, shift));
        cipherTextOuputWriter.flush();

    }

    static int[] generateKey() throws FileNotFoundException {
        System.out.println("Введите длину ключа.");
        int size = INPUT.nextInt();
        PrintWriter printWriter = new PrintWriter(new File("key.txt"));
        int[] key = new int[size];
        for (int i = 0; i < size; i++) {
            key[i] = i;
        }
        MyUtils.shuffleArray(key);
        printWriter.println(size);
        for (int i = 0; i < size; i++) {
            printWriter.println(key[i]);
            printWriter.flush();
        }
        printWriter.flush();
        return key;
    }

    static String code(String text, int[] key) {
        int size = key.length;
        String swappedString = "";
        for (int i = 0; i < text.length() / size - 1; i++) {
            String local = text.substring(i * size, (i + 1) * size);
            for (int aKey : key) {
                swappedString += local.charAt(aKey);
            }
        }
        return swappedString;
    }

    static String decode(String text, int[] key) {
        int size = key.length;
        int[] nKey = new int[size];
        for (int i = 0; i < size; i++) {
            nKey[key[i]] = i;
        }


        String swappedString = "";
        for (int i = 0; i < text.length() / size - 1; i++) {
            String local = text.substring(i * size, (i + 1) * size);

            for (int k = 0; k < size; k++) {
                swappedString += local.charAt(nKey[k]);
            }
        }
        return swappedString;
    }


}
