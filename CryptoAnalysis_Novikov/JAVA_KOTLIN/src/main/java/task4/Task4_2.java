package task4;

import cipher.VigenereCipher;
import org.apache.commons.math3.stat.inference.ChiSquareTest;
import util.FrequencyAnalyzer;
import util.MapUtil;
import util.MyConstants;
import util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Task4_2 {

    public static final String OPEN_TEXT_INPUT_FILE_PATH = "resources/task4/small_open_text.txt";
    public static final String ENCRYPTED_FILE_PATH = "resources/task4/encrypted.txt";
    public static final String ALPHABET_FREQUENCY_PATH = "resources/task4/alphabet_frequency.txt";

    public static final String ENCRYPTION_KEY = "CRYPT";
    public static final int ENCRYPTION_KEY_LENGTH = ENCRYPTION_KEY.length();

    public static final String ALPHABET = MyConstants.ENGLISH_ALPHABET_EXTENDED;

    private static final ChiSquareTest CHI_SQUARE_TEST = new ChiSquareTest();


    public static void main(String[] args) throws FileNotFoundException {
        encryptText();

        breakVigenere();
    }

    private static void breakVigenere() throws FileNotFoundException {
        String encryptedText = StringUtils.getInputFromFileWithSpaces(ENCRYPTED_FILE_PATH);
        Scanner scanner = new Scanner(System.in);
        int keyLength = ENCRYPTION_KEY_LENGTH;
        List<String> subEncryptedTexts = splitTextWithKeyLen(encryptedText,keyLength);
        String myAlphabet = getAlphabet();

        double[] input = getInputFrequency(new File(ALPHABET_FREQUENCY_PATH));
        for (int i = 0; i < input.length; i++) {
            input[i] = input[i]*100.0;
        }

        for (String subEncryptedText : subEncryptedTexts) {
            assert myAlphabet != null;
            HashMap<Double,Character> frequencies = new HashMap<>();
            for (char c : myAlphabet.toCharArray()) {
                String decryptedSubText = VigenereCipher.decrypt(subEncryptedText,Character.toString(c),myAlphabet);
                double[] result =getDoubleFromMap(FrequencyAnalyzer.calculateLetterFrequency(decryptedSubText, myAlphabet));
                long [] longResult = new long[result.length];
                for (int i = 0; i < result.length; i++) {
                    longResult[i] = (long) Math.floor(result[i]*100.0);
                }
                double stat = CHI_SQUARE_TEST.chiSquare(input,longResult);

                frequencies.put(stat,c);
            }
            Set<Double> set = new TreeSet<>(frequencies.keySet());
            for (Double value : set) {
                System.out.print(frequencies.get(value)+" ");
            }
            System.out.println();
        }

    }

    private static void encryptText() throws FileNotFoundException {
        String openText = StringUtils.getInputFromFileWithSpaces(OPEN_TEXT_INPUT_FILE_PATH);
        String encryptedText = VigenereCipher.encrypt(openText, ENCRYPTION_KEY, getAlphabet());

        PrintWriter printWriter = new PrintWriter(ENCRYPTED_FILE_PATH);
        printWriter.print(encryptedText);
        printWriter.flush();
        printWriter.close();
    }

    private static String getAlphabet() {
        try {
            Scanner scanner = new Scanner(new File(ALPHABET_FREQUENCY_PATH));
            StringBuilder alphabetBuilder = new StringBuilder();
            while (scanner.hasNextLine()) {
                alphabetBuilder.append(scanner.next());
                scanner.nextLine();
            }
            return alphabetBuilder.toString();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<String> splitTextWithKeyLen(String text, int keyLength) {
        List<String> result = new ArrayList<>();
        StringBuilder[] stringBuilders = new StringBuilder[keyLength];

        for (int i = 0; i < keyLength; i++) {
            stringBuilders[i] = new StringBuilder();
        }

        for (int i = 0; i < text.length(); i++) {
            stringBuilders[i%keyLength].append(text.charAt(i));
        }

        for (StringBuilder stringBuilder : stringBuilders) {
            result.add(stringBuilder.toString());
        }
        return result;
    }

    private  static double[] getInputFrequency(File al) throws FileNotFoundException {
        Scanner scanner = new Scanner(al);
        Map<Character,Double> map = new TreeMap<>();
        while (scanner.hasNextLine()) {
            final char key = scanner.next().charAt(0);
            final double value = Double.parseDouble(scanner.next().replace("%",""));
            map.put(key,value);
            scanner.nextLine();
        }

        return    getDoubleFromMap(map);
    }

    private static double[] getDoubleFromMap(Map<Character, Double> map) {
        double result [] = new double[map.keySet().size()];
        int i = 0;
        for (Character character : map.keySet()) {
            result[i] = map.get(character);
            i++;
        }
        return result;
    }
}
