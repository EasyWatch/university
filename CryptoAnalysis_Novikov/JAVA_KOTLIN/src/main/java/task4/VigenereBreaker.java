package task4;

import org.apache.commons.math3.stat.inference.ChiSquareTest;
import util.MyConstants;
import util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class VigenereBreaker {
    private static final ChiSquareTest CHI_SQUARE_TEST = new ChiSquareTest();
    private static String ALPHABET = MyConstants.ENGLISH_ALPHABET;

    public static void breakVigenereCipherByKeyLength(File cipherTextFile,File alphabetFrequency, int keyLength) throws FileNotFoundException {
        Map<Character, Double> alphabetFrequencyMap = getInputFrequency(alphabetFrequency);
        StringBuilder alphabet = new StringBuilder("");
        alphabetFrequencyMap.keySet().stream().forEach(alphabet::append);
        ALPHABET =alphabet.toString();
        String cipherText = StringUtils.getInputFromFile(cipherTextFile);
        List<String> subCipherTexts = splitCipherText(cipherText,keyLength);

        for (String subCipherText : subCipherTexts) {
            List<Character> possibleKeys = breakVigenereSubCipher(subCipherText,alphabetFrequencyMap);
        }


    }

    private static List<Character> breakVigenereSubCipher(String subCipherText, Map<Character, Double> alphabetFrequencyMap) {
        return null;
    }

    private static List<String> splitCipherText(String cipherText, int keyLength) {
        final int additionalSymbols = cipherText.length() % keyLength;
        if (additionalSymbols != 0) {
            System.err.println("WARN! Длина шифротекста не кратна длине ключа");
            for (int i = 0; i < additionalSymbols; i++) {
                cipherText+=ALPHABET.charAt(0);
            }
        }

        ArrayList<String> subCiphers = new ArrayList<>();
        for (int i = 0; i < keyLength; i++) {
            subCiphers.add("");
        }

        for (int i = 0; i < cipherText.length(); i++) {
            char letter = cipherText.charAt(i);
            final int position = i % keyLength;
            final String subCipher = subCiphers.get(position);
            if (subCipher != null) {
                subCiphers.set(position, subCipher + letter);
            }
            else {
                subCiphers.set(position, "" + letter);
            }
        }
        return subCiphers;
    }

    private  static Map<Character, Double> getInputFrequency(File al) throws FileNotFoundException {
        Scanner scanner = new Scanner(al);
        Map<Character,Double> map = new TreeMap<>();
        while (scanner.hasNextLine()) {
            final char key = scanner.next().charAt(0);
            final double value = Double.parseDouble(scanner.next().replace("%",""));
            map.put(key,value);
            scanner.nextLine();
        }
        return map;
    }

    public static void main(String[] args) throws FileNotFoundException {
        File inputTextFile = new File("resources/task4/cryptanalysis/input.txt");
        File frequencyFile = new File("resources/task4/cryptanalysis/english_frequency_letter.txt");

        VigenereBreaker.breakVigenereCipherByKeyLength(inputTextFile, frequencyFile, 5);

    }

}
