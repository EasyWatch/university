package task4;


import util.FrequencyAnalyzer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Map;

public class Task4_1 {

    public static final String BIG_OPEN_TEXT_FILE_PATH = "resources/task4/big_open_text.txt";
    public static final String ALPHABET_FREQUENCY_FILE_PATH = "resources/task4/alphabet_frequency.txt";

    public static void main(String[] args) throws FileNotFoundException {
        Map<Character, Double> frequencyMap = FrequencyAnalyzer.calculateLetterFrequency(new File(BIG_OPEN_TEXT_FILE_PATH));

        PrintWriter printWriter = new PrintWriter(new File(ALPHABET_FREQUENCY_FILE_PATH));
        frequencyMap.forEach((letter, frequency) -> {
            printWriter.println(String.format("%s %5.2f", letter, frequency) + "%");
        });
        printWriter.flush();

        System.out.println("Таблица частот языка открытых сообщений успешно вычислена");
    }

}
