package task6;

import cipher.VigenereCipher;
import util.MyUtils;
import util.StringUtils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Task6_2 {

    public static final String OPEN_TEXT_INPUT_FILE_PATH = "resources/task6/small_open_text.txt";
    public static final String ENCRYPTED_INPUT_FILE_PATH = "resources/task6/encrypted.txt";
    public static final String H0_INPUT_FILE_PATH = "resources/task6/h0_output.txt";

    public static final String HD_OUTPUT_FILE_PATH = "resources/task6/hd_output.txt";

    public static final String ENCRYPTION_KEY = "VIGENERE";
    public static final String ALPHABET = " (),-.1234?ABCDEFGHIJKLMNOPQRSTUVWXYZ—’“”";


    public static void main(String[] args) throws FileNotFoundException {
        encryptText();

        findEncryptionKey();
    }

    private static void encryptText() throws FileNotFoundException {
        String openText = StringUtils.getInputFromFileWithSpaces(OPEN_TEXT_INPUT_FILE_PATH);
        String encryptedText = VigenereCipher.encrypt(openText, ENCRYPTION_KEY, ALPHABET);

        PrintWriter printWriter = new PrintWriter(ENCRYPTED_INPUT_FILE_PATH);
        printWriter.print(encryptedText);
        printWriter.flush();
        printWriter.close();
    }

    private static void findEncryptionKey() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите значение периода d(длина ключа):\nd = ");
        int d = Integer.parseInt(scanner.nextLine());;

        List<Double> h0List = MyUtils.readH0List(H0_INPUT_FILE_PATH);
        String encryptedText = StringUtils.getInputFromFileWithSpaces(ENCRYPTED_INPUT_FILE_PATH);


    }

}
