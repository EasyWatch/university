package task6;

import util.FrequencyAnalyzer;
import util.MapUtil;
import util.MyUtils;
import util.StringUtils;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class Task6_1 {

    public static final String BIG_OPEN_TEXT_FILE_PATH  = "resources/task6/big_open_text.txt";
    public static final String ALPHABET_FREQUENCY_OUTPUT_FILE_PATH = "resources/task6/alphabet_frequency.txt";
    public static final String H0_OUTPUT_FILE_PATH = "resources/task6/h0_output.txt";

    public static void main(String[] args) throws FileNotFoundException {
        String openText = StringUtils.getInputFromFileWithSpaces(BIG_OPEN_TEXT_FILE_PATH);
        System.out.println(StringUtils.getAlphabetFromText(openText));
        Map<Character,Double> frequencyMap = FrequencyAnalyzer.calculateLetterFrequency(openText, StringUtils.getAlphabetFromText(openText));
        frequencyMap = MapUtil.sortByValue(frequencyMap);
        printFrequencyMap(frequencyMap);

        List<Double> h0list = MyUtils.calculateH0(frequencyMap);
        printH0List(h0list);
    }

    private static void printFrequencyMap(Map<Character, Double> frequencyMap) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(ALPHABET_FREQUENCY_OUTPUT_FILE_PATH);
        frequencyMap.forEach((letter,frequency) -> {printWriter.println(String.format("%s %5.2f",letter,frequency)+"%");});
        printWriter.flush();
    }

    private static void printH0List(List<Double> h0list) throws FileNotFoundException {
        PrintWriter printWriter = new PrintWriter(H0_OUTPUT_FILE_PATH);
        for (int i = 0; i < h0list.size(); i++) {
            printWriter.println(String.format("P%d = %5.2f",i,h0list.get(i)));
        }
        printWriter.flush();
    }
}
