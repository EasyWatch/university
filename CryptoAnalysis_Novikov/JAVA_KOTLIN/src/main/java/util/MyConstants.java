package util;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class MyConstants {

    public static final String ENGLISH_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static final String ENGLISH_ALPHABET_EXTENDED = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 .,:;\"-—'()“”";

    public static final String RUSSIAN_ALPHABET = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";


}
