package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class MyUtils {

    public static double calculateIndexOfCoincidence(String a, String b) {
        double count = 0;
        if (a.length() != b.length()) {
            System.err.println("Warn! Длины последовательностей не совпадают");
        }
        int n = Math.min(a.length(), b.length());
        for (int i = 0; i < n; i++) {
            count += (a.charAt(i) == b.charAt(i)) ? 1 : 0;
        }
        return count / (double) n;
    }

    public static double calculateAverageIndexOfCoincidence(String a, String b) {
        char[][] s = new char[2][];
        s[0] = a.toCharArray();
        s[1] = b.toCharArray();
        int[][] cnt = new int[2][(1 << 16)];
        for (int i = 0; i < 2; i++) {
            for (char c : s[i]) {
                cnt[i][c]++;
            }
        }
        double result = 0;
        for (int i = 0; i < (1 << 16); i++) {
            result += cnt[0][i] * cnt[1][i];
        }
        return result / ((double) a.length() * (double) b.length());
    }

    public static ArrayList<Double> calculateH0(Map<Character, Double> frequencyMap) throws FileNotFoundException {
        final int m = frequencyMap.size();
        ArrayList<Double> frequencyList = new ArrayList<>(frequencyMap.values());
        ArrayList<Double> h0list = new ArrayList<>(m);
        for (int j = 0; j < m; j++) {
            double p_j = 0.0;
            for (int i = 0; i < m - j; i++) {
                p_j = frequencyList.get(i)*frequencyList.get(i+j)+p_j;
            }
            h0list.add(p_j);
        }
        return h0list;
    }

    public static List<Double> readH0List(String inputFilePath) {
        try {
            Scanner scanner = new Scanner(new File(inputFilePath));
            ArrayList<Double> h0List = new ArrayList<>();
            while (scanner.hasNextLine()) {
                h0List.add(Double.parseDouble(scanner.nextLine().split("=")[1]));
            }
            return h0List;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static int[] shuffleArray(int[] array)
    {
        int n = array.length;
        Random random = ThreadLocalRandom.current();
        for (int i = array.length - 1; i > 0; i--)
        {
           swap(array,i,random.nextInt(n-1));
        }

        int[] ans = new int[n];
        ans[0] = array[0];
        for (int i = 1; i < n; i++) {
            ans[array[i-1]] = array[i];
        }
        return ans;
    }

    private static void swap(int [] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }



}


