package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class StringUtils {

    public static String getInputFromFile(File file) {
        try {
            Scanner scanner = new Scanner(file);
            StringBuilder stringBuilder = new StringBuilder();
            while (scanner.hasNextLine())
            {
                stringBuilder.append(scanner.nextLine().toUpperCase().replace(" ",""));
            }
            return stringBuilder.toString();
        } catch (FileNotFoundException e) {
            System.err.println(String.format("WARN!! Файл \'%s\' не был найден",file.getPath()));
            System.exit(-1);
        }
        return null;
    }

    public static String getInputFromFileWithSpaces(File file) {
        try {
            Scanner scanner = new Scanner(file);
            StringBuilder stringBuilder = new StringBuilder();
            while (scanner.hasNextLine())
            {
                stringBuilder.append(scanner.nextLine().toUpperCase());
            }
            return stringBuilder.toString();
        } catch (FileNotFoundException e) {
            System.err.println(String.format("WARN!! Файл \'%s\' не был найден",file.getPath()));
            System.exit(-1);
        }
        return null;
    }

    public static String getInputFromFile(String filePathName) {
        return getInputFromFile(new File(filePathName));
    }

    public static String getInputFromFileWithSpaces(String filePathName) {
        return getInputFromFileWithSpaces(new File(filePathName));
    }

    public static String getAlphabetFromText(String text) {
        Set<Character> letters = new TreeSet<>();
        for (char c : text.toCharArray()) {
            letters.add(c);
        }
        StringBuilder result = new StringBuilder();
        letters.stream().forEach(result::append);
        return result.toString();
    }

}
