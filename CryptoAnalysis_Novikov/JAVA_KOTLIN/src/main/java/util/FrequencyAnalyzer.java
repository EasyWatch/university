package util;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.TreeMap;


public class FrequencyAnalyzer {

    static Map<Character, Double> calculateLetterFrequency(File textFile,String defaultAlphabet) throws FileNotFoundException
    {
        String text = StringUtils.getInputFromFile(textFile);
        return calculateLetterFrequency(text,defaultAlphabet);
    }

    public static Map<Character, Double> calculateLetterFrequency(File textFile) throws FileNotFoundException
    {
        return calculateLetterFrequency(textFile,null);
    }

    public static Map<Character, Double> calculateLetterFrequency(String text, String defaultAlphabet) throws FileNotFoundException {
        int count = 0;
        final TreeMap<Character, Double> letterCountMap = new TreeMap<>();

        if (defaultAlphabet != null) {
            for (char letter : defaultAlphabet.toCharArray()) {
                letterCountMap.put(letter,0.0);
            }
        }

        for (int i = 0; i < text.length(); i++) {
            final char c = text.charAt(i);
            count++;
            final Double val = letterCountMap.get(c);
            if (val != null) {
                letterCountMap.put(c, val + 1.0);
            } else {
                letterCountMap.put(c, 1.0);
            }
        }

        for (Character letter : letterCountMap.keySet()) {
            final Double letterCount = letterCountMap.get(letter);
            final double letterFrequency = 100.0 * letterCount / count;
            letterCountMap.put(letter, letterFrequency);
        }

        return letterCountMap;
    }


}

