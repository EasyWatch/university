package util;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Evgeniy Strepetov (estrepetov@griddynamics.com)
 */
public class MathUtils {

    public static final Random Random = ThreadLocalRandom.current();

    public static int gcd(int a, int b) {
        int tmp;
        while (b != 0) {
            tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }
}
